package org.viva.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/info")
public class InfoAction {

	@RequestMapping("/index")
	public String index(HttpServletRequest request, Model model) {
		model.addAttribute("s", "ddd");
		return "index";
	}
}
