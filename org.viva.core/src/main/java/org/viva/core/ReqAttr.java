package org.viva.core;

import org.viva.core.api.SkyEye;

/**
 * request参数管理
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class ReqAttr {

	public static final String URL = "_SKYEYE_FILTER_URL_";
	public static final String CLASS = "_SKYEYE_FILTER_CLASS_";
	public static final String METHOD = "_SKYEYE_FILTER_METHOD_";
	public static final String EXCEPTION = "_SKYEYE_FILTER_EXCEPTION_";
	public static final String ACTIONTIME = "_SKYEYE_ACTIONTIME_";
	public static final String LOG = "_SKYEYE_FILTER_LOG_";

	@SuppressWarnings("unchecked")
	public static <T> T getAttribute(String key) {
		return (T) SkyEye.getRequest().getAttribute(key);
	}

	public static void setAttribute(String key, Object value) {
		SkyEye.getRequest().setAttribute(key, value);
	}
}
