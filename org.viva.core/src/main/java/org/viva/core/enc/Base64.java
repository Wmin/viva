package org.viva.core.enc;
//package com.viva.core.enc;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import org.bouncycastle.util.encoders.UrlBase64;
//import org.springframework.util.FileCopyUtils;
//
//public class Base64 {
//
//	public String imgEncodeStr(InputStream is) throws IOException {
//		return encodeStr(FileCopyUtils.copyToByteArray(is));
//	}
//
//	public String encodeStr(String str) {
//		return encodeStr(str.getBytes());
//	}
//
//	public String encodeStr(byte[] b) {
//		return new String(Base64.encode(b));
//	}
//
//	public String decodeStr(byte[] b) {
//		return new String(UrlBase64.decode(b));
//	}
//
//	public String decodeStr(String str) {
//		return new String(UrlBase64.decode(str));
//	}
//
//	@SuppressWarnings("unchecked")
//	public <T> T Base64_Object(String str) throws Exception {
//		byte[] obj = UrlBase64.decode(str);
//		ByteArrayInputStream baos = new ByteArrayInputStream(obj);
//		BufferedInputStream zipOut = new BufferedInputStream(baos);
//		ObjectInputStream os = new ObjectInputStream(zipOut);
//		return (T) os.readObject();
//	}
//
//	public String encodeObject(Object obj) throws IOException {
//		if (obj == null) return null;
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		BufferedOutputStream zipOut = new BufferedOutputStream(baos);
//		ObjectOutputStream os = new ObjectOutputStream(zipOut);
//		os.writeObject(obj);
//		os.flush();
//		os.close();
//		zipOut.close();
//		return encodeStr(baos.toByteArray());
//	}
//
//}
