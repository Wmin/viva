package org.viva.core.enc;

import java.security.MessageDigest;

import org.viva.core.Log;

public class MD5 {

	public String digest(String str) {
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md5 = MessageDigest.getInstance("md5");
			md5.update(str.getBytes());
			byte[] array = md5.digest();
			for (int x = 0; x < 16; x++) {
				if ((array[x] & 0xff) < 0x10) {
					sb.append("0");
				}
				sb.append(Long.toString(array[x] & 0xff, 16));
			}
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
		}
		return sb.toString();
	}

	public static String pwd(String pwd) {
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md5 = MessageDigest.getInstance("md5");
			md5.update(pwd.getBytes());
			byte[] array = md5.digest();
			for (int x = 0; x < 16; x++) {
				if ((array[x] & 0xff) < 0x10) {
					sb.append("0");
				}
				sb.append(Long.toString(array[x] & 0xff, 16));
			}
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
		}
		return sb.toString().toUpperCase();
	}

}
