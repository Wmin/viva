package org.viva.core.exception;

/**
 * action重要异常需记录日志
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class ActionImpException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ActionImpException(String msg) {
		super(msg);
	}

	public ActionImpException(String msg, Exception e) {
		super(msg, e);
	}

}
