package org.viva.core.exception;

/**
 * 系统异常
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class SystemException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SystemException(String s) {
		super(s);
	}

	public SystemException(Exception e) {
		super(e);
	}

	public SystemException(String s, Exception e) {
		super(s, e);
	}
}
