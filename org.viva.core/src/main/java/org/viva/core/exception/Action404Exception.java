package org.viva.core.exception;

/**
 * action请求地址不存在
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class Action404Exception extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public Action404Exception(String msg) {
		super(msg);
		System.out.println(msg);
	}

	public Action404Exception(String msg, Exception e) {
		super(msg, e);
		System.out.println(msg);
	}

	public Action404Exception(Exception e) {
		super(e);
	}

}
