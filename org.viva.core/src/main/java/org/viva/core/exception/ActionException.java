package org.viva.core.exception;

/**
 * action请求异常
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class ActionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ActionException(String msg) {
		super(msg);
	}

	public ActionException(String msg, Exception e) {
		super(msg, e);
	}

}
