package org.viva.core.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

public class ExceptionUtil {

    public static String getInfo(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        StringBuffer sb = new StringBuffer();
        e.printStackTrace(pw);
        BufferedReader br = new BufferedReader(new StringReader(sw.toString()));
        String str = null;
        sb.append("msg:" + e.getMessage());
        while (true) {
            try {
                str = br.readLine();
                if (str == null) break;
                sb.append("\n" + str);
            } catch (IOException e1) {
            }
        }
        return sb.toString();
    }

    public static String getInfoImp(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        StringBuffer sb = new StringBuffer();
        e.printStackTrace(pw);
        BufferedReader br = new BufferedReader(new StringReader(sw.toString()));
        String str = null;
        sb.append("msg:" + e.getMessage());
        while (true) {
            try {
                str = br.readLine();
                if (str == null) break;
                sb.append("\n" + str);
            } catch (IOException e1) {
            }
        }
        return sb.toString();
    }
}
