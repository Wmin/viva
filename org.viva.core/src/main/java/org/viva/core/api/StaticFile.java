package org.viva.core.api;

import java.io.File;

import org.springframework.util.FileCopyUtils;
import org.viva.core.Log;
import org.viva.core.exception.SystemException;
import org.viva.core.util.UTIL;

/**
 * 二进制文件静态化基础类<br />
 * 此静态化模式以生成静态文件为主，主要适用于使用模版导出的资料（合同附件），频繁查看偶尔修改的固定文件<br />
 * <ul>
 * <li>PDF电子合同</li>
 * <li>档案</li>
 * <li>....</li>
 * </ul>
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public abstract class StaticFile {

	/**
	 * 获取文件字节数组
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public byte[] get(String id) {
		return getByte(id);
	}

	/**
	 * 获取文件字节数组
	 * 
	 * @param id
	 * @return
	 */
	public byte[] getByte(String id) {
		try {
			File file = getFile(id);
			byte[] o = null;
			if (file != null && file.exists()) {
				o = FileCopyUtils.copyToByteArray(file);
				if (o != null && o.length > 0) { return o; }
			}
			o = update(id);
			{
				new File(getDir()).mkdirs();
			}
			if (o != null) FileCopyUtils.copy(o, file);
			return o;
		} catch (Exception e) {
			throw new SystemException("", e);
		}
	}

	private String getDir() {
		// String dir = (String) (Config.get("static.file.path") == null ?
		// "/temp/" : Config.get("static.file.path"));
		// dir = dir.endsWith("[/\\]") ? dir : (dir + File.separator);
		return "";// dir;
	}

	/**
	 * 获取文件
	 * 
	 * @param id
	 * @return
	 */
	public File getFile(String id) {
		if (id == null) return null;
		File file = new File(getDir() + UTIL.MD5.digest(getClass() + ":" + id));
		if (!file.exists()) {
			try {
				new File(getDir()).mkdirs();
				byte[] b = update(id);
				if (b == null) return null;
				FileCopyUtils.copy(update(id), file);
			} catch (Exception e) {
				Log.error(e.getMessage(), e);
			}
		}
		return file;
	}

	/**
	 * 清理缓存
	 * 
	 * @param id
	 */
	public void clean(String id) {
		if (id == null) return;
		File f = getFile(id);
		if (f != null && f.exists()) getFile(id).delete();
	}

	/**
	 * 用于更新静态文件<br />
	 * 需要实现该方法，当数据存在修改的时候需要调用该方法更新静态文件，第一次查看会自动加载该方法初始化静态文件
	 * 
	 * @param id
	 * @return
	 */
	public abstract byte[] update(String id);

}
