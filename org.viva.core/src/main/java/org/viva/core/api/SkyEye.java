package org.viva.core.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.viva.core.Web;

/**
 * <h3>天眼平台</h3>
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class SkyEye {

	/**
	 * 请求本地化
	 * 
	 * @param request
	 */
	public static void setRequest(HttpServletRequest request) {
		Web.setRequest(request);
	}

	/**
	 * 获取当前请求对象
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		return Web.getRequest();
	}

	/**
	 * 答复本地化
	 * 
	 * @param response
	 */
	public static void setResponse(HttpServletResponse response) {
		Web.setResponse(response);
	}

	/**
	 * 获取当前答复对象
	 * 
	 * @return
	 */
	public static HttpServletResponse getResponse() {
		return Web.getResponse();
	}

	/**
	 * 清除本地化数据
	 */
	public static void clean() {
		Web.clean();
	}

	/**
	 * 清除session数据
	 */
	public static void cleanHttpSession() {
		Web.cleanHttpSession();
	}

}
