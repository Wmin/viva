package org.viva.core.api;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.springframework.util.FileCopyUtils;
import org.viva.core.util.UTIL;

/**
 * 二进制文件静态化基础类<br />
 * 此静态化模式以生成静态页面为主，主要适用于频繁查看偶尔修改的固定文件<br />
 * <ul>
 * <li>客户资料</li>
 * <li>资信调查报告</li>
 * <li>项目基础信息</li>
 * <li>....</li>
 * </ul>
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public abstract class StaticHtml {

	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String get(String id) throws Exception {
		String dir = "";//(String) (Config.get("static.file.path") == null ? "/temp/" : Config.get("static.file.path"));
		dir = dir.endsWith("[/\\]") ? dir : (dir + File.separator);
		File file = new File(dir + UTIL.MD5.digest(getClass() + ":" + id));
		String o = null;
		if (file.exists()) {
			o = FileCopyUtils.copyToString(new FileReader(file));
			if (o != null) { return o; }
		}
		o = update(id);
		{
			new File(dir).mkdirs();
		}
		FileCopyUtils.copy(o, new FileWriter(file));
		return o;
	}

	/**
	 * 清理缓存
	 * 
	 * @param id
	 */
	public void clean(String id) {
		String dir = null;//TODO 增加缓存数据路径
		File file = new File(dir + UTIL.MD5.digest(getClass() + ":" + id));
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * 用于更新静态文件<br />
	 * 需要实现该方法，当数据存在修改的时候需要调用该方法更新静态页面，第一次查看会自动加载该方法初始化静态页面
	 * 
	 * @param id
	 * @return
	 */
	public abstract String update(String id);

}
