package org.viva.core.api;

import java.util.HashMap;
import java.util.Map;

/**
 * <h3>action 返回ajax请求封装json格式</h3> { flag : true, data : data, msg : msg }
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class Ajax {

	public static Map<String, Object> exec() {
		return exec(true, null, null);
	}

	public static Map<String, Object> exec(Object data) {
		return exec(true, data, null);
	}

	public static Map<String, Object> ReplyAjax(boolean flag, Object data) {
		return exec(flag, data, null);
	}

	public static Map<String, Object> ReplyAjax(boolean flag, String msg) {
		return exec(flag, null, msg);
	}

	public static Map<String, Object> exec(boolean flag, Object data, String msg) {
		Map<String, Object> m = new HashMap<>();
		m.put("flag", flag);
		m.put("data", data);
		m.put("msg", msg);
		return m;
	}

}
