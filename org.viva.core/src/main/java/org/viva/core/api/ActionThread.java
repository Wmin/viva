package org.viva.core.api;

import java.util.Date;

public class ActionThread {
	private Thread thread;
	private Date start;
	private String action;

	public ActionThread() {
		thread = Thread.currentThread();
	}

	public Thread getThread() {
		return thread;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
