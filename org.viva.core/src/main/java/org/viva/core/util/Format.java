package org.viva.core.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Format {

	/**
	 * 格式化日期
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String date(Object obj, String pattern) {
		if (obj == null) return "";
		return new SimpleDateFormat(pattern).format(obj);
	}

	/**
	 * 格式化日期
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @throws ParseException
	 * @date 2012-5-5
	 */
	public Date toDate(String obj, String pattern) throws ParseException {
		if (obj == null) return null;
		return new SimpleDateFormat(pattern).parse(obj);
	}

	/**
	 * 格式化为长日期
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String date(Object obj) {
		if (obj == null) return "";
		return DateFormat.getDateInstance(DateFormat.LONG, Locale.CHINA).format(obj);
	}

	/**
	 * 格式化为长日期
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String date(long obj) {
		return DateFormat.getDateInstance(DateFormat.FULL, Locale.CHINA).format(new Date(obj));
	}

	/**
	 * 格式化为长日期
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String date(long obj, String str) {
		return date(new Date(obj), str);
	}

	/**
	 * 格式化为时间
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String time(Object obj) {
		if (obj == null) return "";
		return DateFormat.getTimeInstance(DateFormat.SHORT, Locale.CHINA).format(obj);
	}

	/**
	 * 格式化为长时间
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String datetime(Object obj) {
		if (obj == null) return "";
		return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.CHINA).format(obj);
	}

	/**
	 * 格式化金钱，人民币
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String currency(Object obj) {
		return currency(obj, Locale.CHINA);
	}

	/**
	 * 格式化金钱
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String currency(Object obj, Locale locale) {
		obj = obj == null || obj.toString().length() == 0 ? "0" : obj;
		return NumberFormat.getCurrencyInstance(locale).format(new BigDecimal(obj.toString()));
	}

	/**
	 * 格式化为百分比
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String percent(Object obj) {
		obj = obj == null || obj.toString().length() == 0 ? 0 : obj;
		if (obj.toString().equalsIgnoreCase("NaN")) return "";
		return NumberFormat.getPercentInstance(Locale.CHINA).format(obj);
	}

	/**
	 * 格式化为整数
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String round(Object obj) {
		obj = obj == null || obj.toString().length() == 0 ? 0 : obj;
		if (obj.toString().equalsIgnoreCase("NaN")) return "NaN";
		return new DecimalFormat("0").format(Double.parseDouble(obj.toString()));
	}

	/**
	 * 格式化为数字
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String number(Object obj, String pattern) {
		obj = obj == null || obj.toString().length() == 0 ? 0 : obj;
		if (obj.toString().equalsIgnoreCase("NaN")) return "NaN";
		return new DecimalFormat(pattern).format(Double.parseDouble(obj.toString()));
	}

	public double scale(double d, int s) {
		BigDecimal bg = new BigDecimal(d);
		return bg.setScale(s, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	// public static void main(String[] args) {
	// System.out.println(date(new Date()));
	// System.out.println(datetime(new Date()));
	// System.out.println(date(new Date(), "yyyy-MM-dd"));
	// System.out.println(currency(1000));
	// System.out.println(round(12939.5911000));
	// System.out.println(percent(0.1111000));
	// System.out.println(currency(1000, Locale.UK));
	// }
}
