package org.viva.core.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import net.sf.json.JSONObject;

import org.springframework.util.FileCopyUtils;

public class IP {

	private final static String URL = "http://ip.taobao.com/service/getIpInfo.php?ip=";

	private static final IP O = new IP();

	private IP() {

	}

	public static IP get() {
		return O;
	}

	public static class Info {
		private String	ip;
		private String	country;
		private String	area;
		private String	region;
		private String	city;
		private String	county;
		private String	isp;
		private String	city_id;
		private String	isp_id;
		private String	county_id;

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getArea() {
			return area;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getRegion() {
			return region;
		}

		public void setRegion(String region) {
			this.region = region;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getCounty() {
			return county;
		}

		public void setCounty(String county) {
			this.county = county;
		}

		public String getIsp() {
			return isp;
		}

		public void setIsp(String isp) {
			this.isp = isp;
		}

		public String getCity_id() {
			return city_id;
		}

		public void setCity_id(String city_id) {
			this.city_id = city_id;
		}

		public String getIsp_id() {
			return isp_id;
		}

		public void setIsp_id(String isp_id) {
			this.isp_id = isp_id;
		}

		public String getCounty_id() {
			return county_id;
		}

		public void setCounty_id(String county_id) {
			this.county_id = county_id;
		}

		@Override
		public String toString() {
			return ip + "|" + country + "|" + area + "|" + region + "|" + city + "|" + county + "|" + isp;
		}

		public void print() {
			System.out.println("ip\t:\t" + ip);
			System.out.println("country\t:\t" + country);
			System.out.println("area\t:\t" + area);
			System.out.println("region\t:\t" + region);
			System.out.println("city\t:\t" + city);
			System.out.println("county\t:\t" + county);
			System.out.println("isp\t:\t" + isp);
		}
	}

	public Info find(String ip) throws IOException {
		URL getUrl = new URL(URL + ip);
		HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
		connection.connect();
		InputStreamReader isr = new InputStreamReader(connection.getInputStream());
		String lines = FileCopyUtils.copyToString(isr);
		isr.close();
		connection.disconnect();
		System.out.println(ip);
		System.out.println(lines);
		System.out.println(JSONObject.fromObject(lines).getJSONObject("data"));
		return (Info) JSONObject.toBean(JSONObject.fromObject(lines).getJSONObject("data"), Info.class);
	}

	public static void main(String[] args) throws IOException {
		Info i = get().find("14.131.118.203");
		i.print();
	}

}
