package org.viva.core.util;

import java.util.regex.Pattern;

public class PatternCheck {

	/** 正则：E-Mail */
	public static final String	EMAIL	= "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

	/** 正则：固定电话 */
	public static final String	PHONE	= "^([0-9]{3,4}-)?[0-9]{7,8}$";

	/** 正则：移动电话 */
	public static final String	MOBIL	= "^1[0-9]{10}$";

	/**
	 * @param regex
	 *            正则表达式字符串
	 * @param str
	 *            要匹配的字符串
	 * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	public boolean check(String regex, String str) {
		return Pattern.compile(regex).matcher(str).matches();
	}

	/**
	 * 验证验证输入汉字
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isChinese(String str) {
		String regex = "^[\u4e00-\u9fa5],{0,}$";
		return check(regex, str);
	}

	/**
	 * 验证数字输入
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isNumber(String str) {
		String regex = "^[0-9]*$";
		return check(regex, str);
	}

	/**
	 * 验证非零的正整数
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isIntNumber(String str) {
		String regex = "^\\+?[1-9][0-9]*$";
		return check(regex, str);
	}

	/**
	 * 验证大写字母
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isUpChar(String str) {
		String regex = "^[A-Z]+$";
		return check(regex, str);
	}

	/**
	 * 验证小写字母
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isLowChar(String str) {
		String regex = "^[a-z]+$";
		return check(regex, str);
	}

	/**
	 * 验证验证输入字母
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isLetter(String str) {
		String regex = "^[A-Za-z]+$";
		return check(regex, str);
	}

	/**
	 * 验证验证输入字符串
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public boolean isLength(String str) {
		String regex = "^.{8,}$";
		return check(regex, str);
	}

	//	public static void main(String[] args) {
	//		String email = "lichaohn@163.com";
	//		System.out.println("[" + check(EMAIL, email) + "]\t" + email);
	//
	//		String phone = "0551-12388283";
	//		System.out.println("[" + check(PHONE, phone) + "]\t" + phone);
	//
	//		String mobil = "15110016868";
	//		System.out.println("[" + check(MOBIL, mobil) + "]\t" + mobil);
	//
	//	}
}
