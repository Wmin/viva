package org.viva.core.util;

public class UTIL {

	public static final DBUtil					DB				= new DBUtil();

	public static final RES						RES				= new RES();

	public static final IMG						IMG				= new IMG();

	public static final Format					FORMAT			= new Format();

	public static final VerifyHandler			VERIFY_HANDLER	= new VerifyHandler();

	public static final PatternCheck			PATTERN_CHECK	= new PatternCheck();

	public static final GenericsUtils			GENERICS		= new GenericsUtils();

	public static final LogFileUtil				LOG_FILE		= new LogFileUtil();

	public static final org.viva.core.enc.MD5	MD5				= new org.viva.core.enc.MD5();

}
