package org.viva.core.util;

import java.sql.Clob;
import java.sql.SQLException;

public class DBUtil {

	/**
	 * 转换clob to String
	 * 
	 * @author <a href='mailto:lichaohn@163.com'>lichao</a>
	 * @date 2012-5-5
	 */
	public String clobToStr(Object obj) throws SQLException {
		if (obj == null) { return ""; }
		Clob club = (Clob) obj;
		return club.getSubString(1, (int) club.length());
	}

}
