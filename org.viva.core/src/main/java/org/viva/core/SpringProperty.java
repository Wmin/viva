//package org.viva.core;
//
//import java.io.IOException;
//import java.util.Properties;
//
//import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
//
///**
// * @author <a href="mailto:lichaohn@163.com">lichao</a>
// */
//public class SpringProperty extends PropertyPlaceholderConfigurer {
//
//	@Override
//	public void loadProperties(Properties props) throws IOException {
//		super.loadProperties(props);
//		props.putAll(Config.getConfig());
//	}
//}
