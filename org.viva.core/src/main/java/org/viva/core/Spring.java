//package org.viva.core;
//
//import java.io.IOException;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.BeanDefinitionStoreException;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.beans.factory.xml.ResourceEntityResolver;
//import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.core.io.ByteArrayResource;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//
//public class Spring {
//
//	private final static Logger			logger	= Logger.getLogger(Spring.class);
//	private static ApplicationContext	context	= null;
//
//	public static void init(String classpath) throws BeanDefinitionStoreException, IOException {
//		classpath = classpath == null ? "classpath*:spring/*.xml" : classpath;
//		logger.debug("init spring application context : " + classpath);
//		context = new ClassPathXmlApplicationContext(classpath.split("[,;]"));
//	}
//
//	@SuppressWarnings("unchecked")
//	public static <T> T getBean(String key) {
//		try {
//			return (T) (context.containsBean(key) ? context.getBean(key) : null);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	@SuppressWarnings("unchecked")
//	public static <T> T getBean(Class<T> cla) {
//		String key = cla.getName();
//		return (T) getBean(key);
//	}
//
//	public static void loadDynamicBean(String classpaths) throws Exception {
//		if (classpaths != null) {
//			final StringBuffer sb = new StringBuffer();
//			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
//			sb.append("<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN//EN\" \"http://www.springframework.org/dtd/spring-beans-2.0.dtd\">\r\n");
//			sb.append("<beans default-autowire=\"byName\" default-lazy-init=\"false\">\r\n");
//			for (String url : classpaths.split("[,;]")) {
//				url = url.replaceAll("[.]", "/") + ".class";
//				Resource[] res = new PathMatchingResourcePatternResolver().getResources("classpath*:" + url);
//				for (Resource resource : res) {
//					sb.append("\r\n");
//					String classname = resource.getURL().toString();
//					classname = classname.split("(classes/)|(!/)")[1];
//					classname = classname.replace("/", ".");
//					classname = classname.replace(".class", "");
//					sb.append("	<bean id=\"" + classname + "\" class=\"" + classname + "\"></bean>\r\n");
//				}
//			}
//			sb.append("</beans>");
//			// logger.debug(sb);
//			XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(
//					(BeanDefinitionRegistry) ((ConfigurableApplicationContext) context).getBeanFactory());
//			beanDefinitionReader.setResourceLoader(context);
//			beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(context));
//			beanDefinitionReader.loadBeanDefinitions(new ByteArrayResource(sb.toString().getBytes("UTF-8")));
//		}
//	}
//}
