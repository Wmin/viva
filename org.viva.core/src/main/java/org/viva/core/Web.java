package org.viva.core;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public final class Web {
	private static ThreadLocal<HttpServletRequest>	localRequest	= new ThreadLocal<HttpServletRequest>();
	private static ThreadLocal<HttpServletResponse>	localResponse	= new ThreadLocal<HttpServletResponse>();

	// public static void init(FilterConfig filterConfig) throws Exception {
	// CentralController.filterConfig = filterConfig;
	// Config.readSystemConfig();
	// Config.readXMLConfig(getRealPath("WEB-INF/config.xml"));
	// Config.readLocalConfig();
	// Filter.init();
	// // if (!Config.IS_FIRST.equals(Config.get("IS_FIRST")))
	// Init.init();
	// }

	// public static void init(String path) throws Exception {
	// Config.readSystemConfig();
	// Config.readXMLConfig(path);
	// Config.readLocalConfig();
	// Filter.init();
	// // if (!Config.IS_FIRST.equals(Config.get("IS_FIRST")))
	// Init.init();
	// }

	// public static String getRealPath(String path) {
	// if (filterConfig == null) return null;
	// return filterConfig.getServletContext().getRealPath(path);
	// }

	public static void setRequest(HttpServletRequest request) {
		localRequest.set(request);
	}

	public static HttpServletRequest getRequest() {
		return localRequest.get();
	}

	public static void setResponse(HttpServletResponse response) {
		localResponse.set(response);
	}

	public static HttpServletResponse getResponse() {
		return localResponse.get();
	}

	public static void clean() {
		localRequest.remove();
		localResponse.remove();
	}

	public static void cleanHttpSession() {
		HttpSession session = getRequest().getSession();
		Enumeration<?> ee = session.getAttributeNames();
		while (ee.hasMoreElements()) {
			String key = (String) ee.nextElement();
			session.removeAttribute(key);
		}
	}

	// public static int m = -1;
	//
	// public static int getM() {
	// return m;
	// }
	//
	// public static void stop() {
	// new Thread(new Runnable() {
	// @Override
	// public void run() {
	// try {
	// m = 60;
	// while (m != 0) {
	// Thread.sleep(1000);
	// m--;
	// System.out.println("即将停机：" + m);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// System.exit(0);
	// }
	// }).start();
	// }

}
