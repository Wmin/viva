package org.viva.core;

import org.apache.log4j.Logger;

/**
 * 全局日志工具，生产环境中可以只通过显示此日志
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class Log {

	public final static Logger logger = Logger.getLogger("skyeye");

	public static void info(Object msg) {
		logger.info(msg);
	}

	public static void error(Object msg, Exception e) {
		logger.error(msg, e);
	}

	public static void error(Object msg) {
		logger.error(msg);
	}

	public static void debug(Object msg) {
		logger.debug(msg);
	}
}
