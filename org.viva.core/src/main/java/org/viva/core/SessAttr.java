package org.viva.core;

import java.io.Serializable;

import org.viva.core.api.SkyEye;

/**
 * session参数管理
 * 
 * @author <a href="mailto:lichaohn@163.com">李超</a>
 */
public class SessAttr {

	@SuppressWarnings("unchecked")
	public static <T> T getAttribute(String key) {
		return (T) SkyEye.getRequest().getSession().getAttribute(key);
	}

	public static void setAttribute(String key, Serializable value) {
		SkyEye.getRequest().getSession().setAttribute(key, value);
	}

	public static void removeAttribute(String key) {
		SkyEye.getRequest().getSession().removeAttribute(key);
	}

}
