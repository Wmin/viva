package org.viva.action.wx;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.viva.service.wx.WxEditTemplateService;

@Controller
public class EditAction {

    @Resource
    private WxEditTemplateService editTemplateService;

    @RequestMapping(value = "/wx/edit", method = RequestMethod.GET)
    public String index(ModelMap model) {
        model.put("wetts", editTemplateService.getTypes());
        return "wx/edit";
    }

}
