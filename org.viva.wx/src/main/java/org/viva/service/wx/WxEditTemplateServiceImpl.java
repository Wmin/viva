package org.viva.service.wx;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.wx.WxEditTemplateType;

@Service(value = "wxEditTemplateService")
public class WxEditTemplateServiceImpl implements WxEditTemplateService {

    @Resource
    private BaseDAO<?> dao;

    @SuppressWarnings("unchecked")
    @Override
    public List<WxEditTemplateType> getTypes() {
        return (List<WxEditTemplateType>) dao.find("from WxEditTemplateType order by sort");
    }

}
