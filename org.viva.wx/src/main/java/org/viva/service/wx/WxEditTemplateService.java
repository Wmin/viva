package org.viva.service.wx;

import java.util.List;

import org.viva.entity.wx.WxEditTemplateType;

public interface WxEditTemplateService {

    List<WxEditTemplateType> getTypes();

}
