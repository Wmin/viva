<!doctype html>
<html>
<head>
<#include "/admin/common/app.ftl">
<script type="text/javascript">
	var UEDITOR_HOME_URL = _basePath + '/res/plugins/ueditor143/';
</script>
<link rel="stylesheet" href="${basePath}/res/plugins/datetimepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="http://cdn.bootcss.com/jquery.form/3.51/jquery.form.min.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/datetimepicker/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/ueditor143/ueditor.config.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/ueditor143/ueditor.all.min.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/jscolor/jscolor.js"></script>
<script type="text/javascript">
	$(function() {
		$("form").submit(function() {
			$("form").ajaxSubmit({
				dataType : "json",
				success : function(json) {
					if (json.flag) {
						alert("操作成功");
						callback();
					} else {
						alert(json.msg);
					}
				}
			});
			return false;
		});
		$('[name=START_DATE]').datetimepicker({
			lang : 'ch',
			format : 'Y-m-d H:i',
			formatDate : 'Y-m-d H:i',
			mask : '9999-19-39 29:59'
		});
		$('[name=END_DATE]').datetimepicker({
			lang : 'ch',
			format : 'Y-m-d H:i',
			formatDate : 'Y-m-d H:i',
			mask : '9999-19-39 29:59'
		});
		var ue = UE.getEditor('MEMO', {
			 toolbars:[[
	            'fullscreen', 'source', '|', 'undo', 'redo', '|',
	            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
	            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
	            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
	            'link', 'unlink', 'anchor', '|'
	        ]],
	        autoHeightEnabled: false,
			initialFrameHeight : 500
		});
		window.clickTemp = function(e) {
			ue.setContent($(e).html(), true);
		}
		function g(o) {
			return document.getElementById(o);
		}
		window.hoverli = function(n) {
			for (var i = 0; i < ${wetts?size}; i++) {
				g('tb_' + i).className = 'aa';
				g('tbc_0' + i).className = 'undis';
			}
			g('tbc_0' + n).className = 'list2';
			g('tb_' + n).className = 'bb';
		}
		hoverli(0);
	});
</script>
<style type="text/css">
<!--
* {
	font-size: 13px;
	font-family: Arial;
}

#nav li {
	list-style: none;
	width: 120px;
	margin-top: 0;
	padding: 15px 6px;
}

#nav li a {
	text-decoration: none;
	color: #666;
}

#nav li a:hover {
	color: #333;
}

.aa {
	border: 1px solid #AAC7E9;
	background: #E8F5FE;
	cursor: hand;
}

.tempitem {
	border: 1px solid #FFF;
	padding: 10px;
}

.tempitem:HOVER {
	border: 1px solid #AAC7E9;
}

.bb {
	border: 1px solid #ff9900;
	border-right: 0px;
	background: #FFFFDD;
	cursor: hand;
	background: #FFFFDD;
}

.cc {
	border-top: 4px solid #ff66ff;
	background: #fcf;
	cursor: hand;
}

.list2 {
	font-size: 13px;
	line-height: 20px;
	padding: 3px;
	text-align: left;
	background: #FFFFFF;
}

.list2   li {
	color: #555;
	font-size: 13px;
	line-height: 24px;
	padding: 0 0 0 10px;
}

.list2    a {
	text-decoration: underline;
}

.lfloat {
	float: left;
}

.rfloat {
	float: right;
}

.ctt {
	padding: 0;
	clear: both;
	border: 1px solid #AAC7E9;
	text-align: left;
	height: 500px;
}

.dis {
	display: block;
}

.undis {
	display: none;
}

li {
	list-style: none;
}

form, ul {
	padding: 0;
	margin: 0;
}
-->
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-6">
				<div style="width: 600px">
					<div class="lfloat" style="width: 126px">
						<ul id="nav">
							<#list wetts as item>
							<li class="aa" id="tb_${item_index}" onClick="x:hoverli(${item_index});">${item.name}</li>
							</#list>
						</ul>
					</div>
					<div class="lfloat" style="width: 450px;">
						<div id="newinfo">
							<div class="ctt list2">
								<#list wetts as item>
								<div class="dis" id="tbc_0${item_index}">
									<#list item.wets as item1>
									<section class="tempitem" onmousedown="clickTemp(this)">${item1.html}</section>
									</#list>
								</div>
								</#list>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<script type="text/plain" id='MEMO' name="MEMO" style="width: 99%"></script>
			</div>
		</div>
	</div>
</body>