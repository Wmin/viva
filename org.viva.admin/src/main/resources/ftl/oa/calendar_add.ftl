<!doctype html>
<html>
<head>
<#include "/admin/common/app.ftl">
<script type="text/javascript">
	var UEDITOR_HOME_URL = _basePath + '/res/plugins/ueditor143/';
</script>
<link rel="stylesheet" href="${basePath}/res/plugins/datetimepicker/jquery.datetimepicker.css">
<script type="text/javascript" src="http://cdn.bootcss.com/jquery.form/3.51/jquery.form.min.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/datetimepicker/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/ueditor143/ueditor.config.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/ueditor143/ueditor.all.min.js"></script>
<script type="text/javascript" src="${basePath}/res/plugins/jscolor/jscolor.js"></script>
<script type="text/javascript">
	$(function() {
		$("form").submit(function() {
			$("form").ajaxSubmit({
				dataType : "json",
				success : function(json) {
					if (json.flag) {
						alert("操作成功");
						callback();
					} else {
						alert(json.msg);
					}
				}
			});
			return false;
		});
		$('[name=startDate]').datetimepicker({
			lang:'ch',
			format:'Y-m-d H:i',
			formatDate:'Y-m-d H:i',
			mask:'9999-19-39 29:59'
		});
		$('[name=endDate]').datetimepicker({
			lang:'ch',
			format:'Y-m-d H:i',
			formatDate:'Y-m-d H:i',
			mask:'9999-19-39 29:59'
		});
		var ue = UE.getEditor('MEMO',{
			toolbars:[['FullScreen', 'Source', 'Undo', 'Redo','Bold','test']],
			initialFrameHeight : 210
		});
	});
</script>
</head>
<body>
	<div>
		<form action="$_basePath/offic/Calendar!doAdd.action" method="post">
			<table style="display:; width: 100%;" id="" class="LayoutTable vtable">
				<colgroup>
					<col width="20%">
					<col width="80%">
				</colgroup>
				<tbody>
					<tr style="display:" _samepair="" class="intervalTR">
						<td colspan="2">
							<table style="width: 100%;" class="LayoutTable">
								<colgroup>
									<col width="50%">
									<col width="50%">
								</colgroup>
								<tbody>
									<tr class="groupHeadHide">
										<td class="interval"><span class="fa fa-th-list"> </span> <span class="e8_grouptitle">基本信息</span></td>
										<td style="text-align: right;">
											<button type="submit">保存</button>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr style="height: 1px; display:" class="Spacing">
						<td colspan="2" class="Line"></td>
					</tr>
					<tr style="display: table-row;" class="items intervalTR">
						<td colspan="2">
							<table style="table-layout: fixed; display:; width: 100%;" id="" class="LayoutTable">
								<colgroup>
									<col width="100px">
								</colgroup>
								<tbody>
									<tr>
										<td colspan="1" class="fieldName">主题：</td>
										<td colspan="1" class="field"><input name='TITLE' style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">时间：</td>
										<td colspan="1" class="field">
											<input name="startDate" value="${param.startDate}" style="width: 105px;"/> 到
											<input name="endDate" style="width: 105px;"/>
											<input class="color" name="COLOR" value="3A87AD">
										</td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">地址：</td>
										<td colspan="1" class="field">
											<input name="ADDRESS" style="width: 405px;"/>
										</td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">内容：</td>
										<td colspan="1" class="field">
											<script type="text/plain" id='MEMO' name="MEMO" style="width: 99%"></script>
										</td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft0">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</body>