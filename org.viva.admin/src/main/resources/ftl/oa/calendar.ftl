<!DOCTYPE html>
<html>
<head>
<#include "/admin/common/app.ftl">
<link rel="stylesheet" href="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/layout-default.min.css">
<link rel="stylesheet" href="${basePath}/res/theme/default/base.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/fullcalendar/2.3.1/fullcalendar.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/tooltipster/3.0.5/css/tooltipster.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/tooltipster/3.0.5/css/themes/tooltipster-light.min.css">
<script type="text/javascript" src="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/jquery.layout.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/moment.js/2.10.3/moment.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/fullcalendar/2.3.1/fullcalendar.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/fullcalendar/2.3.1/lang/zh-cn.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/tooltipster/3.0.5/js/jquery.tooltipster.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('body').layout({
			applyDemoStyles : false,
			spacing_open : 0,
			resizable : false,
			north : {
				size : 60,
				closable : false,
				resizable : false
			},
			west__size : 225
		});
		var tipsy = null;
		$(window).keypress(function() {
			if(tipsy != undefined)tipsy.tooltipster('hide');
	   	});
		$('#calendar').fullCalendar({
			header : {
				left : 'prev,next today',
				center : 'title',
				right : 'month,basicWeek,basicDay'
			},
			eventClick : function(event, element) {
				var t = $(this);
				jQuery.ajax({
					url : "${basePath}/offic/Calendar!get4tip.action",
					data : {
						ID : event.tid
					},
					dataType : "json",
					success : function(json){
						console.info(json);
						var data = json.data;
						var content = $("<div>").css("width","400px");
						var title = $("<div>").text(data.TITLE);
						var sj = $("<div>").text("时间："+data.START_DATE+" 到 "+data.END_DATE);
						content.append(title).append(sj);
						event.tipsy = t.tooltipster({
							trigger:"click",
							content:content
						});
						event.tipsy.tooltipster('show');
					}
				});
			},
			dayClick : function(date, allDay, jsEvent, view) {
				var e = new Date().getTime();
				if(e - jsEvent.ctime < 1000 * 0.5){
					if(jsEvent.cc == date.format('YYYY-MM-DD HH:mm')) toAdd(date.format('YYYY-MM-DD HH:mm'));
				}
				jsEvent.ctime = e;
				jsEvent.cc = date.format('YYYY-MM-DD HH:mm');
			},
			events : function(start, end, timezone, callback) {
				$("#calendar").fullCalendar('removeEvents');
				$.ajax({
					url : '${basePath}/oa/calendar/list',
					dataType : 'json',
					data : {
						startDate : start.format("YYYY-MM-DD HH:mm"),
						endDate : end.format("YYYY-MM-DD HH:mm")
					},
					success : function(json) {
						var events = [];
						for (var i = 0; i < json.length; i++) {
							events.push({
								tid : json[i].id,
								title : json[i].title,
								start : json[i].startDate,
								end : json[i].endDate,
								color :  "#"+json[i].color
							});
						}
						callback(events);
					}
				});
			},
			editable : false,
			eventLimit : true
		});
	});
	function toAdd(START_DATE){
		var url = "${basePath}/oa/calendar/add?";
		if(START_DATE != undefined ){
			url = url + "startDate="+START_DATE;
		}
		var dialog = top.openDialog({
			url : url,
			name : '新建事务',
			width : '600',
			height: '400',
		},function(){
			dialog.close();
			$('#calendar').fullCalendar('refetchEvents');
		});
	}
</script>
<style type="text/css">
#calendar {
	max-width: 99%;
	margin: 10px auto;
}
</style>
</head>
<body>
	<div class="ui-layout-north" style="padding: 0">
		<div class="chead">
			<div class="cicon" style="background-image: url(http://e8.weaver.com.cn/js/tabs/images/nav/default_wev8.png);"></div>
			<div class="cultab">
				<div class="cnavtab">
					<span id="objName" style="max-width: 1076px;">工作日程</span>
				</div>
				<div>
					<ul class="ctab_menu" style="width: 805px;">
						<li>设定权限名称和描述</li>
						<li><a href="javascript:void(0);" onclick="toAdd();">新建事务</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-west" style="padding: 0; border-right: 1px solid #DADADA;">c</div>
	<div class="ui-layout-center" style="padding: 0;">
		<div id='calendar'></div>
	</div>
</body>
</html>