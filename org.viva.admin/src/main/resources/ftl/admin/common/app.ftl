<#-- 取得 应用的绝对根路径 -->
<#assign basePath=request.contextPath>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8">
<link rel="stylesheet" href="${basePath}/res/plugins/vtable/vtable.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script type="text/javascript">
	var _basePath = '${basePath}';
</script>
<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
<script src="${basePath}/res/plugins/zdialog/zDialog.js"></script>
<script src="${basePath}/res/plugins/zdialog/zDrag.js"></script>
<script type="text/javascript">
	function openDialog(option, callback) {
		var diag = new Dialog();
		if (option.name)
			diag.Title = option.name;
		if (option.url)
			diag.URL = option.url;
		if (option.width)
			diag.Width = option.width;
		if (option.height)
			diag.Height = option.height;
		diag.show();
		if (callback)
			diag.innerFrame.contentWindow.callback = callback;
		diag.innerFrame.contentWindow.diag = diag;
		return diag;
	}
</script>
