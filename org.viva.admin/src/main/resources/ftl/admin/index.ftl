<!doctype html>
<html lang="zh">
<head>
<title>${website.ws_name}</title>
<#include "/admin/common/app.ftl">
<link rel="stylesheet" href="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/layout-default.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/jquery.perfect-scrollbar/0.5.9/perfect-scrollbar.min.css">
<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
<script src="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/jquery.layout.min.js"></script>
<script src="http://cdn.bootcss.com/jquery.perfect-scrollbar/0.5.9/perfect-scrollbar.min.js"></script>
<script src="${basePath}/res/plugins/zdialog/zDialog.js"></script>
<script src="${basePath}/res/plugins/zdialog/zDrag.js"></script>
<script src="${basePath}/res/admin.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="${basePath}/res/theme/default/admin-index.css" />
<style type="text/css">
.ui-layout-pane {
	background: none repeat scroll 0 0 #fff;
	border: 0;
	overflow: hidden;
	padding: 0px;
}
</style>
</head>
<body>
	<div class="ui-layout-north" style="padding: 0;">
		<div class='head'>
			<div style="float: left; padding: 0 10px; text-align: center;" class="logo">
				<div class="logoArea">
					<span>${website.ws_name}</span>
				</div>
			</div>
			<div style="float: left;" id='top_menu'></div>
			<div style="float: right; background-color: #959595; padding: 0 10px;">
				<a href="${basePath}/logout"><i class="fa fa-sign-out"></i> 退出</a>
			</div>
			<div style="float: right; margin-right: 10px; line-height: 36px;">
				<div class="btn-group">
					<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><i class="fa fa-user"></i> ${user.code} <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right" role="menu">
						<li><a href="${basePath}/ucenter/Ucenter.action" target="mframe"><i class="fa fa-user"></i> 个人中心<li><a href="#"><i class="fa fa-yelp"></i> 风格</a></li>
						<li class="divider"></li>
						<li><a href="javascript:void(0);" onclick="about();"><i class="fa fa-info-circle"></i> 关于</a></li>
					</ul>
				</div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<div class="ui-layout-west" style="padding: 0;">
		<div id="left_menu" style="border-right: 1px #DDDDDD solid;">
			<div style="height: 40px; line-height: 40px; padding-left: 20px;">
				<label id='lm_title'></label> <span class="fa fa-angle-double-left oo_o btn"></span>
			</div>
			<div id='lmdiv' style="height: 100%"></div>
		</div>

	</div>
	<div class="ui-layout-center" style="padding: 0; overflow: hidden;">
		<iframe name='mframe' id='mframe' width="100%" height="100%" frameborder='0' src=""></iframe>
	</div>
</body>