<!doctype html>
<html>
<head>
<title>Login</title>
<#assign basePath=request.contextPath>
<link rel="shortcut icon" href="${basePath}/favicon.ico">
<link rel="icon" type="image/gif" href="${basePath}/animated_favicon.gif">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta charset="utf-8">
<script type="text/javascript">
	var _basePath = '${basePath}';
</script>
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel=stylesheet type=text/css href="res/login/login.css" />
<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
<script type="text/javascript">
	$(function() {
	});
</script>
<script type="text/javascript" src="http://qzonestyle.gtimg.cn/qzone/openapi/qc_loader.js" data-appid="101173979" 
data-redirecturi="http://www.dowdybear.com/login/qq" charset="utf-8"></script>
</head>
<body id="loginFrame">
	<div id="header">
		<div id="logo">
			<a></a>
		</div>
	</div>
	<div id="loginBox">
		<div id="loginBoxHeader"></div>
		<div id="loginBoxBody">
			<ul class="floatLeft">
				<li>
					<h4>请用您的注册账号登录</h4>
				</li>
				<form action="${basePath}/login" method="post" id="login">
					<li>
						<p>帐号:</p> <input type="text" name="code" size="30" maxlength="16" class="textInput" value="admin" autocomplete="off">
					</li>
					<li>
						<p>密码:</p> <input type="password" name="password" size="30" maxlength="80" class="textInput" value="1" autocomplete="off"> <a
						target="_blank" href="" class="highlight">忘记密码？</a>
					</li>
					<li class="highlight">
						<input type="submit" value="登录" onclick="this.blur();" id="loginBtn" />
						<a target="_blank" href="" id="regBtn">注册新账号</a>
					</li>
					<li>
						其他登录方式：
						<span id="qqLoginBtn">qqlogin</span>
						<script type="text/javascript">
						    QC.Login({
						       btnId:"qqLoginBtn"    //插入按钮的节点id
							});
						</script>
					</li>
					<li></li>
				</form>
			</ul>
			<div class="floatRight">
				欢迎访问UI制造者，在这里您可以欣赏优秀的UI设计作品，同时还能够学习经验技能，下载分享精美的设计素材。<br>U制造者-专注于UI界面设计
			</div>
			<br clear="all">
		</div>
		<div id="loginBoxFooter"></div>
	</div>
	<div id="footer">
		<a target="_blank" href="http://www.dowdybear.com" style="color: #fff"><i class="fa fa-copyright"></i> 邋遢熊 www.dowdybear.com 2015 Co., Ltd</a>
	</div>
</body>