<!doctype html>
<html>
<head>
<#include "/admin/common/app.ftl">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script type="text/javascript">
	$(function() {
		$(".fa-hover").hover(function() {
			$(this).css("background","linear-gradient(to bottom,#beebff 0,#a8e4ff 100%)");
		}, function() {
			$(this).css("background", "#ffffff");
		});
		$(".fa-hover").click(function() {
			var class_str = $(this).find("i").attr("class");
			callback(class_str);
			diag.close();
		});
	});
</script>
<style type="text/css">
.fa{
	font-size: 14px;
}
</style>
</head>
<body class="fa container-fluid">
	<section id="web-application" class="mainParts">
		<h2 class="page-header">网页</h2>
		<div class="row span12">
			<div class="fa-hover col-xs-4">
				<a><i class="fa-adjust"></i> adjust</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-anchor"></i> anchor</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-archive"></i> archive</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-area-chart"></i> area-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows"></i> arrows</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-h"></i> arrows-h</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-v"></i> arrows-v</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-asterisk"></i> asterisk</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-at"></i> at</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-automobile"></i> automobile <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ban"></i> ban</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bank"></i> bank <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bar-chart"></i> bar-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bar-chart-o"></i> bar-chart-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-barcode"></i> barcode</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bars"></i> bars</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bed"></i> bed</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-beer"></i> beer</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bell"></i> bell</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bell-o"></i> bell-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bell-slash"></i> bell-slash</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bell-slash-o"></i> bell-slash-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bicycle"></i> bicycle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-binoculars"></i> binoculars</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-birthday-cake"></i> birthday-cake</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bolt"></i> bolt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bomb"></i> bomb</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-book"></i> book</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bookmark"></i> bookmark</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bookmark-o"></i> bookmark-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-briefcase"></i> briefcase</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bug"></i> bug</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-building"></i> building</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-building-o"></i> building-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bullhorn"></i> bullhorn</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bullseye"></i> bullseye</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bus"></i> bus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cab"></i> cab <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-calculator"></i> calculator</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-calendar"></i> calendar</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-calendar-o"></i> calendar-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-camera"></i> camera</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-camera-retro"></i> camera-retro</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-car"></i> car</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-down"></i> caret-square-o-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-left"></i> caret-square-o-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-right"></i> caret-square-o-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-up"></i> caret-square-o-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cart-arrow-down"></i> cart-arrow-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cart-plus"></i> cart-plus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc"></i> cc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-certificate"></i> certificate</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check"></i> check</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-circle"></i> check-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-circle-o"></i> check-circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-square"></i> check-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-square-o"></i> check-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-child"></i> child</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle"></i> circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-o"></i> circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-o-notch"></i> circle-o-notch</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-thin"></i> circle-thin</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-clock-o"></i> clock-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-close"></i> close <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cloud"></i> cloud</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cloud-download"></i> cloud-download</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cloud-upload"></i> cloud-upload</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-code"></i> code</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-code-fork"></i> code-fork</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-coffee"></i> coffee</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cog"></i> cog</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cogs"></i> cogs</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-comment"></i> comment</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-comment-o"></i> comment-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-comments"></i> comments</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-comments-o"></i> comments-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-compass"></i> compass</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-copyright"></i> copyright</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-credit-card"></i> credit-card</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-crop"></i> crop</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-crosshairs"></i> crosshairs</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cube"></i> cube</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cubes"></i> cubes</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cutlery"></i> cutlery</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dashboard"></i> dashboard <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-database"></i> database</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-desktop"></i> desktop</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-diamond"></i> diamond</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dot-circle-o"></i> dot-circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-download"></i> download</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-edit"></i> edit <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ellipsis-h"></i> ellipsis-h</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ellipsis-v"></i> ellipsis-v</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-envelope"></i> envelope</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-envelope-o"></i> envelope-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-envelope-square"></i> envelope-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eraser"></i> eraser</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-exchange"></i> exchange</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-exclamation"></i> exclamation</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-exclamation-circle"></i> exclamation-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-exclamation-triangle"></i> exclamation-triangle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-external-link"></i> external-link</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-external-link-square"></i> external-link-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eye"></i> eye</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eye-slash"></i> eye-slash</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eyedropper"></i> eyedropper</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fax"></i> fax</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-female"></i> female</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fighter-jet"></i> fighter-jet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-archive-o"></i> file-archive-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-audio-o"></i> file-audio-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-code-o"></i> file-code-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-excel-o"></i> file-excel-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-image-o"></i> file-image-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-movie-o"></i> file-movie-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-pdf-o"></i> file-pdf-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-photo-o"></i> file-photo-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-picture-o"></i> file-picture-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-powerpoint-o"></i> file-powerpoint-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-sound-o"></i> file-sound-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-video-o"></i> file-video-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-word-o"></i> file-word-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-zip-o"></i> file-zip-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-film"></i> film</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-filter"></i> filter</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fire"></i> fire</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fire-extinguisher"></i> fire-extinguisher</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flag"></i> flag</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flag-checkered"></i> flag-checkered</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flag-o"></i> flag-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flash"></i> flash <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flask"></i> flask</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-folder"></i> folder</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-folder-o"></i> folder-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-folder-open"></i> folder-open</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-folder-open-o"></i> folder-open-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-frown-o"></i> frown-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-futbol-o"></i> futbol-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gamepad"></i> gamepad</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gavel"></i> gavel</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gear"></i> gear <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gears"></i> gears <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-genderless"></i> genderless <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gift"></i> gift</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-glass"></i> glass</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-globe"></i> globe</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-graduation-cap"></i> graduation-cap</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-group"></i> group <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hdd-o"></i> hdd-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-headphones"></i> headphones</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heart"></i> heart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heart-o"></i> heart-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heartbeat"></i> heartbeat</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-history"></i> history</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-home"></i> home</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hotel"></i> hotel <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-image"></i> image <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-inbox"></i> inbox</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-info"></i> info</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-info-circle"></i> info-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-institution"></i> institution <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-key"></i> key</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-keyboard-o"></i> keyboard-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-language"></i> language</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-laptop"></i> laptop</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-leaf"></i> leaf</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-legal"></i> legal <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-lemon-o"></i> lemon-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-level-down"></i> level-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-level-up"></i> level-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-life-bouy"></i> life-bouy <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-life-buoy"></i> life-buoy <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-life-ring"></i> life-ring</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-life-saver"></i> life-saver <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-lightbulb-o"></i> lightbulb-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-line-chart"></i> line-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-location-arrow"></i> location-arrow</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-lock"></i> lock</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-magic"></i> magic</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-magnet"></i> magnet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mail-forward"></i> mail-forward <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mail-reply"></i> mail-reply <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mail-reply-all"></i> mail-reply-all <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-male"></i> male</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-map-marker"></i> map-marker</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-meh-o"></i> meh-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-microphone"></i> microphone</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-microphone-slash"></i> microphone-slash</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus"></i> minus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus-circle"></i> minus-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus-square"></i> minus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus-square-o"></i> minus-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mobile"></i> mobile</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mobile-phone"></i> mobile-phone <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-money"></i> money</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-moon-o"></i> moon-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mortar-board"></i> mortar-board <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-motorcycle"></i> motorcycle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-music"></i> music</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-navicon"></i> navicon <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-newspaper-o"></i> newspaper-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paint-brush"></i> paint-brush</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paper-plane"></i> paper-plane</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paper-plane-o"></i> paper-plane-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paw"></i> paw</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pencil"></i> pencil</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pencil-square"></i> pencil-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pencil-square-o"></i> pencil-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-phone"></i> phone</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-phone-square"></i> phone-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-photo"></i> photo <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-picture-o"></i> picture-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pie-chart"></i> pie-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plane"></i> plane</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plug"></i> plug</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus"></i> plus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-circle"></i> plus-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-square"></i> plus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-square-o"></i> plus-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-power-off"></i> power-off</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-print"></i> print</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-puzzle-piece"></i> puzzle-piece</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-qrcode"></i> qrcode</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-question"></i> question</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-question-circle"></i> question-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-quote-left"></i> quote-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-quote-right"></i> quote-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-random"></i> random</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-recycle"></i> recycle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-refresh"></i> refresh</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-remove"></i> remove <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-reorder"></i> reorder <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-reply"></i> reply</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-reply-all"></i> reply-all</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-retweet"></i> retweet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-road"></i> road</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rocket"></i> rocket</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rss"></i> rss</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rss-square"></i> rss-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-search"></i> search</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-search-minus"></i> search-minus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-search-plus"></i> search-plus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-send"></i> send <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-send-o"></i> send-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-server"></i> server</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share"></i> share</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-alt"></i> share-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-alt-square"></i> share-alt-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-square"></i> share-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-square-o"></i> share-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-shield"></i> shield</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ship"></i> ship</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-shopping-cart"></i> shopping-cart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sign-in"></i> sign-in</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sign-out"></i> sign-out</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-signal"></i> signal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sitemap"></i> sitemap</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sliders"></i> sliders</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-smile-o"></i> smile-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-soccer-ball-o"></i> soccer-ball-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort"></i> sort</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-alpha-asc"></i> sort-alpha-asc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-alpha-desc"></i> sort-alpha-desc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-amount-asc"></i> sort-amount-asc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-amount-desc"></i> sort-amount-desc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-asc"></i> sort-asc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-desc"></i> sort-desc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-down"></i> sort-down <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-numeric-asc"></i> sort-numeric-asc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-numeric-desc"></i> sort-numeric-desc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sort-up"></i> sort-up <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-space-shuttle"></i> space-shuttle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-spinner"></i> spinner</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-spoon"></i> spoon</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-square"></i> square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-square-o"></i> square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star"></i> star</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star-half"></i> star-half</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star-half-empty"></i> star-half-empty <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star-half-full"></i> star-half-full <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star-half-o"></i> star-half-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-star-o"></i> star-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-street-view"></i> street-view</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-suitcase"></i> suitcase</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sun-o"></i> sun-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-support"></i> support <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tablet"></i> tablet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tachometer"></i> tachometer</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tag"></i> tag</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tags"></i> tags</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tasks"></i> tasks</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-taxi"></i> taxi</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-terminal"></i> terminal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-thumb-tack"></i> thumb-tack</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-thumbs-down"></i> thumbs-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-thumbs-o-down"></i> thumbs-o-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-thumbs-o-up"></i> thumbs-o-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-thumbs-up"></i> thumbs-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ticket"></i> ticket</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-times"></i> times</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-times-circle"></i> times-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-times-circle-o"></i> times-circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tint"></i> tint</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-down"></i> toggle-down <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-left"></i> toggle-left <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-off"></i> toggle-off</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-on"></i> toggle-on</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-right"></i> toggle-right <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-up"></i> toggle-up <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-trash"></i> trash</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-trash-o"></i> trash-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tree"></i> tree</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-trophy"></i> trophy</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-truck"></i> truck</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tty"></i> tty</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-umbrella"></i> umbrella</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-university"></i> university</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-unlock"></i> unlock</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-unlock-alt"></i> unlock-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-unsorted"></i> unsorted <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-upload"></i> upload</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-user"></i> user</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-user-plus"></i> user-plus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-user-secret"></i> user-secret</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-user-times"></i> user-times</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-users"></i> users</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-video-camera"></i> video-camera</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-volume-down"></i> volume-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-volume-off"></i> volume-off</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-volume-up"></i> volume-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-warning"></i> warning <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wheelchair"></i> wheelchair</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wifi"></i> wifi</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wrench"></i> wrench</a>
			</div>

		</div>

	</section>

	<section id="transportation" class="mainParts">
		<h2 class="page-header">运输</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-ambulance"></i> ambulance</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-automobile"></i> automobile <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bicycle"></i> bicycle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bus"></i> bus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cab"></i> cab <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-car"></i> car</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fighter-jet"></i> fighter-jet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-motorcycle"></i> motorcycle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plane"></i> plane</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rocket"></i> rocket</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ship"></i> ship</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-space-shuttle"></i> space-shuttle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-subway"></i> subway</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-taxi"></i> taxi</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-train"></i> train</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-truck"></i> truck</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wheelchair"></i> wheelchair</a>
			</div>

		</div>

	</section>


	<section id="gender" class="mainParts">
		<h2 class="page-header">性别</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-thin"></i> circle-thin</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-genderless"></i> genderless <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mars"></i> mars</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mars-double"></i> mars-double</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mars-stroke"></i> mars-stroke</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mars-stroke-h"></i> mars-stroke-h</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mars-stroke-v"></i> mars-stroke-v</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-mercury"></i> mercury</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-neuter"></i> neuter</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-transgender"></i> transgender</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-transgender-alt"></i> transgender-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-venus"></i> venus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-venus-double"></i> venus-double</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-venus-mars"></i> venus-mars</a>
			</div>

		</div>

	</section>


	<section id="file-type" class="mainParts">
		<h2 class="page-header">文件类型</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-file"></i> file</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-archive-o"></i> file-archive-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-audio-o"></i> file-audio-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-code-o"></i> file-code-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-excel-o"></i> file-excel-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-image-o"></i> file-image-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-movie-o"></i> file-movie-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-o"></i> file-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-pdf-o"></i> file-pdf-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-photo-o"></i> file-photo-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-picture-o"></i> file-picture-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-powerpoint-o"></i> file-powerpoint-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-sound-o"></i> file-sound-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-text"></i> file-text</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-text-o"></i> file-text-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-video-o"></i> file-video-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-word-o"></i> file-word-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-zip-o"></i> file-zip-o <span class="text-muted">(别名)</span></a>
			</div>

		</div>

	</section>

	<section id="spinner" class="mainParts">
		<h2 class="page-header">可旋转</h2>

		<div class="alert alert-success">
			<ul class="fa-ul">
				<li><i class="fa-info-circle fa-lg fa-li"></i> 这些图标在<code>fa-spin</code>类的作用下表现优异。您可以查阅 <a href="#spinning" class="alert-link">可旋转图标案例</a>.
				</li>
			</ul>
		</div>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-o-notch"></i> circle-o-notch</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cog"></i> cog</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gear"></i> gear <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-refresh"></i> refresh</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-spinner"></i> spinner</a>
			</div>

		</div>
	</section>

	<section id="form-control" class="mainParts">
		<h2 class="page-header">表单</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-square"></i> check-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-check-square-o"></i> check-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle"></i> circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-circle-o"></i> circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dot-circle-o"></i> dot-circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus-square"></i> minus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-minus-square-o"></i> minus-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-square"></i> plus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-square-o"></i> plus-square-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-square"></i> square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-square-o"></i> square-o</a>
			</div>

		</div>
	</section>

	<section id="payment" class="mainParts">
		<h2 class="page-header">支付</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-amex"></i> cc-amex</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-discover"></i> cc-discover</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-mastercard"></i> cc-mastercard</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-paypal"></i> cc-paypal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-stripe"></i> cc-stripe</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-visa"></i> cc-visa</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-credit-card"></i> credit-card</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-google-wallet"></i> google-wallet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paypal"></i> paypal</a>
			</div>

		</div>

	</section>

	<section id="chart" class="mainParts">
		<h2 class="page-header">图表</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-area-chart"></i> area-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bar-chart"></i> bar-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bar-chart-o"></i> bar-chart-o <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-line-chart"></i> line-chart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pie-chart"></i> pie-chart</a>
			</div>

		</div>

	</section>

	<section id="currency" class="mainParts">
		<h2 class="page-header">货币</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-bitcoin"></i> bitcoin <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-btc"></i> btc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cny"></i> cny <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dollar"></i> dollar <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eur"></i> eur</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-euro"></i> euro <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gbp"></i> gbp</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ils"></i> ils</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-inr"></i> inr</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-jpy"></i> jpy</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-krw"></i> krw</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-money"></i> money</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rmb"></i> rmb <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rouble"></i> rouble <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rub"></i> rub</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ruble"></i> ruble <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rupee"></i> rupee <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-shekel"></i> shekel <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sheqel"></i> sheqel <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-try"></i> try</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-turkish-lira"></i> turkish-lira <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-usd"></i> usd</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-won"></i> won <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-yen"></i> yen <span class="text-muted">(别名)</span></a>
			</div>

		</div>

	</section>

	<section id="text-editor" class="mainParts">
		<h2 class="page-header">文本编辑</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-align-center"></i> align-center</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-align-justify"></i> align-justify</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-align-left"></i> align-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-align-right"></i> align-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bold"></i> bold</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chain"></i> chain <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chain-broken"></i> chain-broken</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-clipboard"></i> clipboard</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-columns"></i> columns</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-copy"></i> copy <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cut"></i> cut <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dedent"></i> dedent <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eraser"></i> eraser</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file"></i> file</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-o"></i> file-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-text"></i> file-text</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-file-text-o"></i> file-text-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-files-o"></i> files-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-floppy-o"></i> floppy-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-font"></i> font</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-header"></i> header</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-indent"></i> indent</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-italic"></i> italic</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-link"></i> link</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-list"></i> list</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-list-alt"></i> list-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-list-ol"></i> list-ol</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-list-ul"></i> list-ul</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-outdent"></i> outdent</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paperclip"></i> paperclip</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paragraph"></i> paragraph</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paste"></i> paste <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-repeat"></i> repeat</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rotate-left"></i> rotate-left <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rotate-right"></i> rotate-right <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-save"></i> save <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-scissors"></i> scissors</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-strikethrough"></i> strikethrough</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-subscript"></i> subscript</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-superscript"></i> superscript</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-table"></i> table</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-text-height"></i> text-height</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-text-width"></i> text-width</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-th"></i> th</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-th-large"></i> th-large</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-th-list"></i> th-list</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-underline"></i> underline</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-undo"></i> undo</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-unlink"></i> unlink <span class="text-muted">(别名)</span></a>
			</div>

		</div>

	</section>

	<section id="directional" class="mainParts">
		<h2 class="page-header">指示方向</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-double-down"></i> angle-double-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-double-left"></i> angle-double-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-double-right"></i> angle-double-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-double-up"></i> angle-double-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-down"></i> angle-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-left"></i> angle-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-right"></i> angle-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angle-up"></i> angle-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-down"></i> arrow-circle-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-left"></i> arrow-circle-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-o-down"></i> arrow-circle-o-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-o-left"></i> arrow-circle-o-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-o-right"></i> arrow-circle-o-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-o-up"></i> arrow-circle-o-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-right"></i> arrow-circle-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-circle-up"></i> arrow-circle-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-down"></i> arrow-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-left"></i> arrow-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-right"></i> arrow-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrow-up"></i> arrow-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows"></i> arrows</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-alt"></i> arrows-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-h"></i> arrows-h</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-v"></i> arrows-v</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-down"></i> caret-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-left"></i> caret-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-right"></i> caret-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-down"></i> caret-square-o-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-left"></i> caret-square-o-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-right"></i> caret-square-o-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-square-o-up"></i> caret-square-o-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-caret-up"></i> caret-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-circle-down"></i> chevron-circle-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-circle-left"></i> chevron-circle-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-circle-right"></i> chevron-circle-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-circle-up"></i> chevron-circle-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-down"></i> chevron-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-left"></i> chevron-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-right"></i> chevron-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-chevron-up"></i> chevron-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hand-o-down"></i> hand-o-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hand-o-left"></i> hand-o-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hand-o-right"></i> hand-o-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hand-o-up"></i> hand-o-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-long-arrow-down"></i> long-arrow-down</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-long-arrow-left"></i> long-arrow-left</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-long-arrow-right"></i> long-arrow-right</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-long-arrow-up"></i> long-arrow-up</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-down"></i> toggle-down <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-left"></i> toggle-left <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-right"></i> toggle-right <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-toggle-up"></i> toggle-up <span class="text-muted">(别名)</span></a>
			</div>

		</div>

	</section>

	<section id="video-player" class="mainParts">
		<h2 class="page-header">视频播放</h2>

		<div class="row span12">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-arrows-alt"></i> arrows-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-backward"></i> backward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-compress"></i> compress</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-eject"></i> eject</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-expand"></i> expand</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fast-backward"></i> fast-backward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-fast-forward"></i> fast-forward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-forward"></i> forward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pause"></i> pause</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-play"></i> play</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-play-circle"></i> play-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-play-circle-o"></i> play-circle-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-step-backward"></i> step-backward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-step-forward"></i> step-forward</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stop"></i> stop</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-youtube-play"></i> youtube-play</a>
			</div>

		</div>

	</section>

	<section id="brand" class="mainParts">
		<h2 class="page-header">标志</h2>

		<div class="alert alert-success">
			<ul class="margin-bottom-none padding-left-lg">
				<li>所有标志图标都分别是其所有者的注册商标。</li>
				<li>使用这些商标并不代表Font Awesome拥有它们。</li>
			</ul>

		</div>

		<div class="alert alert-warning">
			<h4>
				<i class="fa-warning"></i> 特别注意!
			</h4>
			Adblock Plus 插件会通过设置“Remove Social Media Buttons”来移除 Font Awesome 的这些标志图标。 然而我们并不会用一些特殊方法来强行显示。如果您认为这是一个错误，请 <a
				href="https://adblockplus.org/en/bugs" class="alert-link" target="_blank">向 Adblock Plus 报告这个问题</a>。 在Adblock Plus修复这个问题之前，您需要自行修改这些图标的类名来解决这个问题。

		</div>

		<div class="row span12 margin-bottom-lg">



			<div class="fa-hover col-xs-4">
				<a><i class="fa-adn"></i> adn</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-android"></i> android</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-angellist"></i> angellist</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-apple"></i> apple</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-behance"></i> behance</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-behance-square"></i> behance-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bitbucket"></i> bitbucket</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bitbucket-square"></i> bitbucket-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-bitcoin"></i> bitcoin <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-btc"></i> btc</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-buysellads"></i> buysellads</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-amex"></i> cc-amex</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-discover"></i> cc-discover</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-mastercard"></i> cc-mastercard</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-paypal"></i> cc-paypal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-stripe"></i> cc-stripe</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-cc-visa"></i> cc-visa</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-codepen"></i> codepen</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-connectdevelop"></i> connectdevelop</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-css3"></i> css3</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dashcube"></i> dashcube</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-delicious"></i> delicious</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-deviantart"></i> deviantart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-digg"></i> digg</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dribbble"></i> dribbble</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-dropbox"></i> dropbox</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-drupal"></i> drupal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-empire"></i> empire</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-facebook"></i> facebook</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-facebook-f"></i> facebook-f <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-facebook-official"></i> facebook-official</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-facebook-square"></i> facebook-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-flickr"></i> flickr</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-forumbee"></i> forumbee</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-foursquare"></i> foursquare</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ge"></i> ge <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-git"></i> git</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-git-square"></i> git-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-github"></i> github</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-github-alt"></i> github-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-github-square"></i> github-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gittip"></i> gittip <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-google"></i> google</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-google-plus"></i> google-plus</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-google-plus-square"></i> google-plus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-google-wallet"></i> google-wallet</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-gratipay"></i> gratipay</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hacker-news"></i> hacker-news</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-html5"></i> html5</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-instagram"></i> instagram</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ioxhost"></i> ioxhost</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-joomla"></i> joomla</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-jsfiddle"></i> jsfiddle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-lastfm"></i> lastfm</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-lastfm-square"></i> lastfm-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-leanpub"></i> leanpub</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-linkedin"></i> linkedin</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-linkedin-square"></i> linkedin-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-linux"></i> linux</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-maxcdn"></i> maxcdn</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-meanpath"></i> meanpath</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-medium"></i> medium</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-openid"></i> openid</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pagelines"></i> pagelines</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-paypal"></i> paypal</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pied-piper"></i> pied-piper</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pied-piper-alt"></i> pied-piper-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pinterest"></i> pinterest</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pinterest-p"></i> pinterest-p</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-pinterest-square"></i> pinterest-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-qq"></i> qq</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ra"></i> ra <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-rebel"></i> rebel</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-reddit"></i> reddit</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-reddit-square"></i> reddit-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-renren"></i> renren</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-sellsy"></i> sellsy</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-alt"></i> share-alt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-share-alt-square"></i> share-alt-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-shirtsinbulk"></i> shirtsinbulk</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-simplybuilt"></i> simplybuilt</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-skyatlas"></i> skyatlas</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-skype"></i> skype</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-slack"></i> slack</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-slideshare"></i> slideshare</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-soundcloud"></i> soundcloud</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-spotify"></i> spotify</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stack-exchange"></i> stack-exchange</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stack-overflow"></i> stack-overflow</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-steam"></i> steam</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-steam-square"></i> steam-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stumbleupon"></i> stumbleupon</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stumbleupon-circle"></i> stumbleupon-circle</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tencent-weibo"></i> tencent-weibo</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-trello"></i> trello</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tumblr"></i> tumblr</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-tumblr-square"></i> tumblr-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-twitch"></i> twitch</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-twitter"></i> twitter</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-twitter-square"></i> twitter-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-viacoin"></i> viacoin</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-vimeo-square"></i> vimeo-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-vine"></i> vine</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-vk"></i> vk</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wechat"></i> wechat <span class="text-muted">(别名)</span></a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-weibo"></i> weibo</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-weixin"></i> weixin</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-whatsapp"></i> whatsapp</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-windows"></i> windows</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-wordpress"></i> wordpress</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-xing"></i> xing</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-xing-square"></i> xing-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-yahoo"></i> yahoo</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-yelp"></i> yelp</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-youtube"></i> youtube</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-youtube-play"></i> youtube-play</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-youtube-square"></i> youtube-square</a>
			</div>

		</div>
	</section>

	<section id="medical" class="mainParts">
		<h2 class="page-header">医疗</h2>

		<div class="row span12">

			<div class="fa-hover col-xs-4">
				<a><i class="fa-ambulance"></i> ambulance</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-h-square"></i> h-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heart"></i> heart</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heart-o"></i> heart-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-heartbeat"></i> heartbeat</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-hospital-o"></i> hospital-o</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-medkit"></i> medkit</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-plus-square"></i> plus-square</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-stethoscope"></i> stethoscope</a>
			</div>

			<div class="fa-hover col-xs-4">
				<a><i class="fa-user-md"></i> user-md</a>
			</div>
			<div class="fa-hover col-xs-4">
				<a><i class="fa-wheelchair"></i> wheelchair</a>
			</div>

		</div>
	</section>
</body>
</html>