<!doctype html>
<html>
<head>
<meta charset="utf-8">
<#include "/admin/common/app.ftl">

<link rel="stylesheet" href="${basePath}/plugins/vtable/vtable.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
<script src="${basePath}/plugins/zdialog/zDialog.js"></script>
<script src="${basePath}/plugins/zdialog/zDrag.js"></script>
<script type="text/javascript">
	function openDialog(option, callback) {
		var diag = new Dialog();
		if (option.name)
			diag.Title = option.name;
		if (option.url)
			diag.URL = option.url;
		if (option.width)
			diag.Width = option.width;
		if (option.height)
			diag.Height = option.height;
		diag.show();
		if (callback)
			diag.innerFrame.contentWindow.callback = callback;
		diag.innerFrame.contentWindow.diag = diag;
		return diag;
	}
</script>

<link rel="stylesheet" href="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/layout-default.min.css">
<link rel="stylesheet" href="${basePath}/res/theme/default/base.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.bootcss.com/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css">
<script type="text/javascript" src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/jquery.layout.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('body').layout({
			applyDemoStyles : false,
			spacing_open : 0,
			resizable : false,
			north : {
				size : 60,
				closable : false,
				resizable : false
			},
			west__size : 225
		});
	});
	$(function() {
		$(".editable").editable({
			mode:"inline"
		});//mode = c === 'inline'
		$("#inplaceediting-enable").click(function() {
			$(".editable").editable('toggleDisabled');
		});
	});
</script>
</head>
<body>
	<div class="ui-layout-north" style="padding: 0">
		<div class="chead">
			<div class="cicon" style="background-image: url(http://e8.weaver.com.cn/js/tabs/images/nav/default_wev8.png);"></div>
			<div class="cultab">
				<div class="cnavtab">
					<span id="objName" style="max-width: 1076px;">网站设置</span>
				</div>
				<div>
					<ul class="ctab_menu" style="width: 805px;">
						<li><a> 功能类(前台) </a> <span class="crightBorder">|</span></li>
						<li><a> 管理类(后台) </a> <span class="crightBorder" style="display: none;">|</span></li>
					</ul>
				</div>
			</div>
			<div class='pull-right' style="margin-top: 10px;">
				<button class='btn btn-primary' id='inplaceediting-enable'>Enable / Disable</button>
			</div>
		</div>
	</div>
	<div class="ui-layout-center" style="padding: 0;">
		<div class='container-fluid'>
			<table class='table table-bordered table-striped' id='inplaceediting-menu' style='margin-top: 20px'>
				<tbody>
					<#list list as item>
					<tr>
						<td style='width: 15%'>${item.name}</td>
						<td style='width: 50%'><a class='editable' href="javascript:void(0)" data-type="text" data-pk="${item.id}" data-name="${item.type}"
							data-url="${basePath}/sys/website/update" data-original-title="类型${item.type}">${item.string_}</a></td>
						<td style='width: 35%'><span class='muted'>${item.memo}</span></td>
					</tr>
					</#list>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>