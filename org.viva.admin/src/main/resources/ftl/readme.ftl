<!DOCTYPE HTML>
<html>
<head>
<title>关于 ecology</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<style>
body{
	font-size: 12px;
}
</style>
</head>
<body>
	<table>
		<tbody>
			<tr class="firstRow">
				<td width="699" valign="top" style="word-break: break-all;">
					<p>VIVA集成开发系统 &nbsp;1.0.0 Snapshot 说明文档</p>
					<p>
						<br />
					</p>
					<p>1.起源</p>
					<p>整合门户、ERP开发所需要用到模块，搭建一个基础开发平台。</p>
					<p>
						<br />
					</p>
					<p>2.功能</p>
					<p>其主要的功能就是,进行后台管理模块整合.</p>
					<p>1)其可以支持多个用户,多个应用,多个模块的权限管理.</p>
					<p>2)集成hibernate,支持mysql/oracle/sqlserver 等多种数据库</p>
					<p>3)采用角色来进行权限的授权,每个用户可以属于多个角色,享有交差权限.</p>
					<p>4)权限检测采用anno注解方式检测.</p>
					<p>5)统一的事件日志管理,所有登陆操作都有安全记录.</p>
					<p>
						<br />
					</p>
					<p>3.开发环境</p>
					<p>eclipse + maven + mysql</p>
					<p>
						<br />
					</p>
					<p>4.初始用户名:admin 密码:1</p>
					<p>
						<br />
					</p>
					<p>5.官方论坛</p>
					<p>
						<br />
					</p>
					<p>6.帮助文档</p>
					<p>
						<br />
					</p>
					<p>7.官方网站</p>
					<p>
						<br />
					</p>
					<p>8.商业合作联系方式</p>
					<p>QQ:181531051</p>
					<p>Email:lichaohn@163.com</p>
					<p>
						<br />
					</p>
					<p>感谢您支持 VIVA集成开发系统(VIVA) 产品。希望我们的努力能为大家提供一个高效快速和强大的开发框架。</p>
				</td>
			</tr>
		</tbody>
	</table>
	<p>
		<br />
	</p>
</body>
</html>