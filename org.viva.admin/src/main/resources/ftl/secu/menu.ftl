<!doctype html>
<html>
<head>
<#include "/admin/common/app.ftl">
<link rel="stylesheet" href="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/layout-default.min.css">
<link rel="stylesheet" href="${basePath}/res/theme/default/base.css">
<link href="http://cdn.bootcss.com/jstree/3.1.0/themes/default/style.min.css" rel="stylesheet">
<script type="text/javascript" src="http://cdn.bootcss.com/jquery-layout/1.3.0-rc-30.79/jquery.layout.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/jstree/3.1.0/jstree.min.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/jquery.form/3.51/jquery.form.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('body').layout({
			applyDemoStyles : false,
			spacing_open : 0,
			resizable : false,
			north : {
				size : 60,
				closable : false,
				resizable : false
			},
			west__size : 225
		});
	});

	function getdetail() {
		var ref = $('#treeDemo').jstree(true);
		var sel = ref.get_selected();
		sel = sel[0];
		if(!sel.length) { showdetail(data);return false; }
		if (typeof (sel) != 'undefined') {
			$.ajax({
				type : "get",
				url : "${basePath}/secu/menu/infoJson",
				data : "id=" + sel,
				dataType : "json",
				success : function(data) {
					showdetail(data);
				}
			});
		} else {
			showdetail(data);
		}
	}
	function showdetail(data) {
		if (data === undefined) {
			$("#menu_info_panel").hide();
			$("#menu_info_panel_warning").show();
		} else {
			$("#menu_info_panel_warning").hide();
			$("#menu_info_panel").show();
			$(".ui-layout-center").find("[name='id']").val(data.id);
			$(".ui-layout-center").find("[name='iconFa']").val(data.iconFa);
			$(".ui-layout-center").find("[name='sort']").val(data.sort);
			$(".ui-layout-center").find("[name='name']").val(data.name);
			$(".ui-layout-center").find("[name='type']").val(data.type);
			$(".ui-layout-center").find("[name='status']").val(data.status);
			$(".ui-layout-center").find("[name='url']").val(data.url);
		}
	}

	$(document).ready(function() {
				jQuery.ajax({
					url : '${basePath}/secu/menu/allJson',
					dataType : 'json',
					success : function(json) {
						if (json.flag) {
							var setting = {
								data : {
									simpleData : {
										enable : true
									}
								}
							};
							var treeDate = [];
							for (var i = 0; i < json.data.length; i++) {
								var jd = json.data[i];
								var ti = {};
								ti.id = jd.id;
								ti.parent = jd.pid > 0 ? jd.pid : "#";
								ti.text = jd.name;
								ti.icon = jd.iconFa;
								treeDate[i] = ti;
							}
							initTree(treeDate);
						}
					}
				});

				function initTree(treeDate) {
					var s = $('#treeDemo').jstree({
								'core' : {
									'data' : treeDate,
									"animation" : 0,
									"check_callback" : true,
									'force_text' : true
								},
								"plugins" : [ "contextmenu", "dnd", "state",
										"types", "wholerow" ],
								"contextmenu" : {
									"items" : {
										"add" : {
											"icon" : "fa fa-new",
											"label" : "添加",
											"action" : function(data) {
												var ref = $('#treeDemo').jstree(true);
												var sel = ref.get_selected();
												sel = sel[0];
												if(!sel.length) { return false; }
												$.ajax({
													url : "${basePath}/secu/menu/save?pid="+sel,
													dataType : "json",
													success : function(json){
														sel = ref.create_node(sel,{id:json.data.id});
													}
												});
											}
										},
										"del" : {
											"icon" : "fa fa-del",
											"label" : "删除",
											"action" : function(data) {
												var ref = $('#treeDemo').jstree(true);
												var sel = ref.get_selected();
												sel = sel[0];
												if(!sel.length) { return false; }
												$.ajax({
													url : "${basePath}/secu/menu/del?id="+sel,
													dataType : "json",
													success : function(json){
														if(!json.flag){alert(json.msg);return;}
														ref.delete_node(sel);
													}
												});
											}
										}
									}
								}
							}).bind('click.jstree', function(event) {
						getdetail();
					});
					setTimeout(getdetail, 500);
				}
			});
	$(function() {

		$(".browseicon").click(function() {
			top.openDialog({
				url : '${basePath}/sys/icon/choose',
				name : '选择icon',
				width : '700',
				height : '650',
			}, function(n) {
				$("[name='iconFa']").val(n);
			});
		});
		
		$("form").submit(function() {
			$("form").ajaxSubmit({
				dataType : "json",
				success : function(json) {
					if (json.flag) {
						var ref = $('#treeDemo').jstree(true);
						ref.rename_node(json.data.id,json.data.name);
						ref.set_icon(json.data.id,json.data.iconFa);
					} else {
						alert(json.msg);
					}
				}
			});
			return false;
		});
	});
</script>
</head>
<body>
	<div class="ui-layout-north" style="padding: 0">
		<div class="chead">
			<div class="cicon" style="background-image: url(http://e8.weaver.com.cn/js/tabs/images/nav/default_wev8.png);"></div>
			<div class="cultab">
				<div class="cnavtab">
					<span id="objName" style="max-width: 1076px;">菜单设置</span>
				</div>
				<div>
					<ul class="ctab_menu" style="width: 805px;">
						<li><a> 功能类(前台) </a> <span class="crightBorder">|</span></li>
						<li><a> 管理类(后台) </a> <span class="crightBorder" style="display: none;">|</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-west" style="padding: 0; border-right: 1px solid #DADADA; overflow: auto;">
		<ul id="treeDemo" style="margin: 0; padding: 0;"></ul>
	</div>
	<div class="ui-layout-center" style="padding: 0;">
		<form action="${basePath}/secu/menu/save" method="post">
			<table style="display:; width: 100%;" id="" class="LayoutTable vtable">
				<colgroup>
					<col width="20%">
					<col width="80%">
				</colgroup>
				<tbody>
					<tr style="display:" _samepair="" class="intervalTR">
						<td colspan="2">
							<table style="width: 100%;" class="LayoutTable">
								<tbody>
									<tr class="groupHeadHide">
										<td class="interval"><span class="fa fa-th-list"> </span> <span class="e8_grouptitle">菜单信息</span></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr style="height: 1px; display:" class="Spacing">
						<td colspan="2" class="Line"></td>
					</tr>
					<tr style="display: table-row;" class="items intervalTR">
						<td colspan="2">
							<table style="table-layout: fixed; display:; width: 100%;" id="" class="LayoutTable">
								<colgroup>
									<col width="100px">
								</colgroup>
								<tbody>
									<tr>
										<td colspan="1" class="fieldName">ID：</td>
										<td colspan="1" class="field"><input name='id' style="width: 400px;" readonly="readonly" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">ICON：</td>
										<td colspan="1" class="field"><input name='iconFa' style="width: 400px;" />
										<a href="javascript:void(0);" class="browseicon"><i class="fa fa-eye"></i></a>
										</td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">NAME：</td>
										<td colspan="1" class="field"><input name="name" style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">SORT：</td>
										<td colspan="1" class="field"><input name="sort" style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">STATUS：</td>
										<td colspan="1" class="field"><input name="status" style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">TYPE：</td>
										<td colspan="1" class="field"><input name="type" style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft18">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName">URL：</td>
										<td colspan="1" class="field"><input name="url" style="width: 400px;" /></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft0">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
									<tr>
										<td colspan="1" class="fieldName"></td>
										<td colspan="1" class="field"><button type="submit">保存</button></td>
									</tr>
									<tr class="Spacing" style="height: 1px !important; display:;">
										<td colspan="2" class="paddingLeft0">
											<div class="intervalDivClass"></div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</body>