<!DOCTYPE HTML>
<html>
<head>
<title>关于 ecology</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<style>
.textfield {
	width: 90%;
	height: auto;
	overflow: auto;
	margin-left: 30px;
	font-size: 12px;
}

#splite {
	width: 25px;
	display: inline;
}

.style01 {
	display: block;
	font-size: 14px;
	color: #1547c8;
	vertical-align: middle;
	margin-left: 30px;
}

.style02 {
	display: block;
	font-size: 14px;
	color: #1d1d1d;
	vertical-align: middle;
	margin-left: 30px;
}

.style03 {
	display: inline-block;
	font-size: 12px;
	color: #000000;
	vertical-align: middle;
	margin-left: 30px;
}

.style04 {
	display: inline-block;
	font-size: 12px;
	color: #6abcfb;
	vertical-align: middle;
}

#container {
	position: relative;
	display: block;
	background: #ffffff;
	width: 100%;
	height: 80px;
	float: left;
}

.logoimage {
	float: left;
	width: 202px;
	height: 45px;
	margin-top: 20px;
	background: url("/images_face/version/logo_wev8.png");
}
</style>

</head>
<body text="#000000" bgcolor="#FFFFFF" scroll="no" topmargin="0" leftmargin="0">
	<div >
		<div id="container">
			<div style="width: 30px !important; height: 1px; float: left;"></div>
			<div class="logoimage"></div>
		</div>
		<div style="clear: both;"></div>
		<div style="background: rgb(204, 204, 204); width: 90%; height: 1px !important; margin-left: 30px;"></div>
		<div style="width: 1px; height: 20px !important; float: left;"></div>
		<div id="container">
			<div>
				<span class="style01">版本:&nbsp;1.0-SNAPSHOT</span>
			</div>
			<div style="width: 1px; height: 5px !important; float: left;"></div>
			<div style="clear: both;"></div>
			<div>
				<span class="style02">授权用户:&nbsp;未授权</span>
			</div>
			<div style="width: 1px; height: 15px !important; float: left;"></div>
			<div style="clear: both;"></div>

			<div class="textfield">
				<div class="splite"></div>
				本软件是基于J2EE的各种技术，B/S模式的三层结构设计完成的，由DOWDYBEAR独立开发。<br>
				<div class="splite"></div>
				本软件的版权属于DOWDYBEAR，未经授权许可不得擅自发布该软件。<br>
				<div class="splite"></div>
				<br>
				<div class="splite"></div>
				警告:本计算机程序受著作权法和国际公约的保护，未经授权擅自复制或散布本程序的部分或全部，将承受严厉的民事和刑事处罚，对已知的违反者将给予法律范围内的全面制裁。
			</div>
			<div style="width: 1px; height: 20px !important; float: left;"></div>
			<div style="clear: both;"></div>
			<div>
				<div style="float: left;">
					<span class="style03">版权所有&nbsp;&nbsp;&copy;&nbsp;&nbsp;DOWDYBEAR.COM</span>
				</div>
				<div style="float: right; margin-right: 35px;">
					<span class="style03">DOWDYBEAR网站:</span>&nbsp;<a target="_blank" href="http://www.dowdybear.com"><span class="style04">www.dowdybear.com</span></a>
				</div>
			</div>
			<div style="width: 1px; height: 20px !important; float: left;"></div>
			<div style="clear: both;"></div>
		</div>
	</div>
</body>
</html>