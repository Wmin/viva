$(document).ready(function() {
	var ff = true;
	var layout = $('body').layout({
		applyDemoStyles : false,
		spacing_open : 0,
		resizable : false,
		showOverflowOnHover : true,
		north : {
			size : 40,
			closable : false,
			resizable : false
		},
	    togglerAlign_closed:"left",//pane关闭时，边框按钮显示的位置  
	    togglerContent_closed:'<span class="fa fa-angle-double-right oo_c"></span>',
	    fxName:"drop",//打开关闭的动画效果  
	    fxSpeed:"slow",//动画速度 
		spacing_closed : 30,
		togglerLength_closed : 35,
		west__size : 225
	});

	$('.oo_o').click(function() {
		if (ff) {
			layout.close('west');
		}
	});
});

$(document).ready(function() {
	var menus;
	$.ajax({
		url : _basePath + '/admin/menu',
		dataType : 'json',
		success : function(json) {
			if (json.flag) {
				menus = json.data;
				for (var i = 0; i < menus.length; i++) {
					var menu = menus[i];
					if (menu.pid > 0)
						continue;
					var sp1 = $('<span>').addClass('fa').addClass(menu.iconFa);
					var md = $('<menu>').addClass(
							'top_menu_item').css('float',
							'left').append(sp1).append(menu.name).attr('sid',menu.id);
					md.click(clickTopMenu);
					$('#top_menu').append(md);
				}
				$('.top_menu_item').get(0).click();
			}
		}
	});

	function clickTopMenu() {
		$('.top_menu_item.on').removeClass('on');
		$(this).addClass('on');
		var id = $(this).attr('sid');
		$('#lmdiv').empty();
		$('#lm_title').text($(this).text());
		for (var i = 0; i < menus.length; i++) {
			var menu = menus[i];
			if (menu.pid != id)
				continue;
			if (menu.url == undefined || menu.url == '')
				continue;
			var md = $('<a>').addClass("menu").attr('href',_basePath + menu.url).attr('target','mframe');
			var sp1 = $('<span>').addClass('fa').addClass(menu.iconFa);
			var sp2 = $('<span>').addClass('mm').text(menu.name);
			md.click(clickMenu);
			$('#lmdiv').append(md.append(sp1).append(sp2));
		}
		$('#left_menu .menu').get(0).click();
		$('#lmdiv').perfectScrollbar('update');
	}

	function clickMenu() {
		$('#left_menu .menu.on').removeClass('on');
		$(this).addClass('on');
	}
});

function openUrl(url) {
	$('#mframe').attr('src', url);
}

function openDialog(option, callback) {
	var diag = new Dialog();
	if (option.name)
		diag.Title = option.name;
	if (option.url)
		diag.URL = option.url;
	if (option.width)
		diag.Width = option.width;
	if (option.height)
		diag.Height = option.height;
	diag.show();
	if (callback)
		diag.innerFrame.contentWindow.callback = callback;
	diag.innerFrame.contentWindow.diag = diag;
	return diag;
}

function about() {
	top.openDialog({
		url : _basePath + '/about',
		name : '关于',
		width : '600',
		height : '350',
	});
}