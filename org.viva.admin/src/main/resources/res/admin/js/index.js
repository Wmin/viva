$(document).ready(function() {
	var ff = true;
	var layout = $('body').layout({
		applyDemoStyles : false,
		spacing_open : 0,
		resizable : false,
		showOverflowOnHover : true,
		north : {
			size : 50,
			closable : false,
			resizable : false
		},
		west__size : 225
	});

	$('.oo').click(function() {
		if (ff) {
			layout.close('west');
		}
	});
});

$(document)
		.ready(
				function() {
					var menus;
					$.ajax({
						url : _basePath + '/admin/menu',
						dataType : 'json',
						success : function(json) {
							if (json.flag) {
								menus = json.data;
								for (var i = 0; i < menus.length; i++) {
									var menu = menus[i];
									if (menu.PID != '0')
										continue;
									var md = $('<menu>').addClass(
											'top_menu_item').css('float',
											'left').text(menu.NAME).attr('sid',
											menu.ID);
									md.click(clickTopMenu);
									$('#top_menu').append(md);
								}
								$('.top_menu_item').get(0).click();
							}
						}
					});

					function clickTopMenu() {
						$('.top_menu_item.on').removeClass('on');
						$(this).addClass('on');
						var id = $(this).attr('sid');
						$('#lmdiv').empty();
						$('#lm_title').text($(this).text());
						for (var i = 0; i < menus.length; i++) {
							var menu = menus[i];
							if (menu.PID != id)
								continue;
							var md = $('<a>').addClass("menu").attr('href', _basePath + menu.URL).attr('target','mframe');
							var sp1 = $('<span>').addClass('fa').addClass(
									menu.ICON_FA);
							var sp2 = $('<span>').addClass('mm')
									.text(menu.NAME);
							md.click(clickMenu);
							$('#lmdiv').append(md.append(sp1).append(sp2));
						}
						$('#left_menu .menu').get(0).click();
						$('#lmdiv').perfectScrollbar('update');
					}

					function clickMenu() {
						$('#left_menu .menu.on').removeClass('on');
						$(this).addClass('on');
					}
				});

function openDialog(option, callback) {
	var diag = new Dialog();
	if (option.name)
		diag.Title = option.name;
	if (option.url)
		diag.URL = option.url;
	if (option.width)
		diag.Width = option.width;
	if (option.height)
		diag.Height = option.height;
	diag.show();
	if (callback)
		diag.innerFrame.contentWindow.callback = callback;
	diag.innerFrame.contentWindow.diag = diag;
}
function about() {
	top.openDialog({
		url : _basePath + '/About.action',
		name : '关于',
		width : '600',
		height : '400',
	});
}