package org.viva.alibaba.aliyun.oss;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.viva.core.Log;
import org.viva.core.util.UTIL;

public class OSSClientProperties {

	private static Logger logger = Logger.getLogger(OSSClientProperties.class);
	
	private static Properties OSSKeyProperties = new Properties();
	// 阿里云是否启用配置
	public static String bucketName = "";
	public static boolean useStatus = false;
	public static String key = "";
	public static String secret = "";
	public static String endPoint = "";

	public static void init(){
		// 生成文件输入流
		InputStream inpf = null;
		try {
			inpf = UTIL.RES.getResource("classpath:OSSKey.properties").getInputStream();
			OSSKeyProperties.load(inpf);
			useStatus = "true".equalsIgnoreCase((String) OSSKeyProperties.get("useStatus")) ? true : false;
			bucketName = (String) OSSKeyProperties.get("bucketName");
			key = (String) OSSKeyProperties.get("key");
			secret = (String) OSSKeyProperties.get("secret");
			endPoint = (String) OSSKeyProperties.get("endPoint");
			Log.info("init aliyun oss success");
		} catch (Exception e) {
			logger.warn("系统未找到指定文件：OSSKey.properties --> 系统按照ueditor默认配置执行。");
		}finally{
			try {
				inpf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	static {
		init();
	}

}
