package org.viva.admin.util.page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Page {
    private int page = 0;

    private int                 total = 0;
    private int                 rows  = 0;
    private Map<String, Object> param = new HashMap<>();

    public int getMaxPage() {
        int maxPage = total / rows;
        maxPage = maxPage + (total % rows > 0 ? 1 : 0);
        return maxPage;
    }

    private List<?> data;
    private List<?> footer;

    public Page() {
    }

    public Page(Map<String, Object> param) {
        addParam(param);
    }

    public void addParam(Map<String, Object> m) {
        if (m != null) {
            this.param = m;
        }
        int page = param.get("page") == null ? 1 : Integer.parseInt(param.get("page").toString());
        int rows = 20;
        if (param.get("rows") == "" || param.get("rows") == null) {
            rows = 20;
        } else {
            rows = Integer.parseInt(param.get("rows").toString());
        }
        param.put("page", page);
        param.put("rows", rows);
        param.put("PAGE_BEGIN", rows * (page - 1));
        param.put("PAGE_END", rows * page);
        this.page = page;
        this.rows = rows;
    }

    public void addDate(List<?> data, int total) {
        this.data = data;
        this.total = total;
        param.put("maxPage", getMaxPage());
    }

    public void addDate(List<?> data, int total, List<?> footer) {
        this.data = data;
        this.total = total;
        this.footer = footer;
        param.put("maxPage", getMaxPage());
    }

    public int getPage() {
        return page;
    }

    public int getRows() {
        return rows;
    }

    public int getTotal() {
        return total;
    }

    public List<?> getData() {
        return data;
    }

    public List<?> getFooter() {
        return this.footer;
    }
}
