package org.viva.secu;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import org.viva.core.SessAttr;
import org.viva.entity.secu.User;

public class Security {

	public static final String	USER		= "_USER_";
	public static final String	TOKEN		= "_TOKEN_";
	public static final String	WATE_TOKEN	= "_WATE_TOKEN_";
	public static final String	TOKEN_COUNT	= "_TOKEN_COUNT_";

	/**
	 * 获取session会话中user对象
	 * 
	 * @return
	 */
	public static User getUser() {
		return (User) SessAttr.getAttribute(USER);
	}

	public static User login(User user) {
		SessAttr.setAttribute(USER, user);
		return user;
	}

	public static void cleanSession(HttpSession session) {
		Enumeration<?> it = session.getAttributeNames();
		while (it.hasMoreElements()) {
			String key = (String) it.nextElement();
			session.removeAttribute(key);
		}
	}

}
