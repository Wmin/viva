package org.viva.entity.secu;

import java.io.Serializable;
import java.util.Set;
import java.util.Vector;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SECU_USER")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User implements Serializable, HttpSessionBindingListener {

    private static final Vector<User>        USERS    = new Vector<User>();
    private static final Vector<HttpSession> SESSIONS = new Vector<HttpSession>();

    private static final long serialVersionUID = 300032295749069472L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long   id;
    private String code;
    private String password;
    private String nickname;

    @JsonIgnore
    @ManyToMany(mappedBy = "users", fetch = FetchType.LAZY)
    private Set<Role> roles;

    public long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public void valueBound(final HttpSessionBindingEvent arg0) {
        USERS.add(this);
        SESSIONS.add(arg0.getSession());
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent arg0) {
        USERS.remove(this);
        SESSIONS.remove(arg0.getSession());
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
