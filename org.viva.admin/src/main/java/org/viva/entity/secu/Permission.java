package org.viva.entity.secu;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SECU_PERMISSION")
public class Permission implements Serializable {

	private static final long serialVersionUID = 2374888932868142641L;

	@Id
	@GeneratedValue
	private long id;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinTable(name = "SECU_ROLE_PERMISSION", joinColumns = {
			@JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "PERMISSION_ID", nullable = true) })
	private Set<Role> roles;

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
