package org.viva.entity.secu;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SECU_ORG")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Org implements Serializable {

    private static final long serialVersionUID = -8948522250943337448L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long     id;
    private String   name;
    private String   sname;
    @Column(name = "ICON_FA")
    private String   iconFa;
    private String   type;
    private String   status;
    private int      sort;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PID")
    private Org      parent;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", fetch = FetchType.LAZY)
    private Set<Org> children = new LinkedHashSet<Org>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconFa() {
        return iconFa;
    }

    public void setIconFa(String iconFa) {
        this.iconFa = iconFa;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Set<Org> getChildren() {
        return children;
    }

    public Org getParent() {
        return parent;
    }

    public void setChildren(Set<Org> children) {
        this.children = children;
    }

    public void setParent(Org parent) {
        this.parent = parent;
    }

}
