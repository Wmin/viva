package org.viva.entity.secu;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SECU_ROLE")
public class Role implements Serializable {

	private static final long serialVersionUID = 2891237399788297260L;

	@Id
	@GeneratedValue
	private long	id;
	private String	name;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinTable(name = "SECU_USER_ROLE", joinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
			@JoinColumn(name = "ROLE_ID", nullable = true) })
	private Set<User> users;

	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
	private Set<Permission> permissions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Collection<String> getPermissionsAsString() {
		return null;
	}

}
