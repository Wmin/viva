package org.viva.service.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.sys.SysLog;

@Service("logService")
public class LogServiceImpl implements LogService {

	@Resource
	private BaseDAO<SysLog> dao;

	@Override
	public void add(SysLog log) {
		dao.save(log);
	}

}
