package org.viva.service.sys;

import java.util.List;
import java.util.Map;

import org.viva.entity.sys.Website;

public interface WebsiteService {

	List<Website> getAll();

	Website get(long id);

	Website get(String code);
	
	Map<String, Object> getCache();

	void rfCache();
}
