package org.viva.service.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.sys.Website;

@Service
public class WebsiteServiceImpl implements WebsiteService {

	private static Map<String, Object> cache = null;

	@Resource
	private BaseDAO<Website> dao;

	@Override
	public Website get(long id) {
		return dao.get(Website.class, id);
	}

	@Override
	public List<Website> getAll() {
		return dao.find("from Website");
	}

	@Override
	public Website get(String code) {
		return dao.get("from Website where code = ?", new String[] { code });
	}

	@Override
	public Map<String, Object> getCache() {
		if (cache == null) {
			rfCache();
		}
		return cache;
	}

	@Override
	public void rfCache() {
		cache = new HashMap<>();
		for (Website website : getAll()) {
			cache.put(website.getCode(), website.getString_());
		}
	}

}
