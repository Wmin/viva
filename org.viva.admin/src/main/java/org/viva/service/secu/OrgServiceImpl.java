package org.viva.service.secu;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.secu.Org;

@Service
public class OrgServiceImpl implements OrgService {

    @Resource
    private BaseDAO<Org> dao;

    @Override
    public List<?> getAllMap() {
        String sql = "SELECT T.ID,T.PID,T.`NAME`,T.ICON_FA FROM SECU_ORG T ORDER BY PID,SORT";
        SQLQuery query = dao.getCurrentSession().createSQLQuery(sql);
        return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
    }

    @Override
    public Org get(long id) {
        return dao.get(Org.class, id);
    }

    @Override
    public void save(Org org) {
        if (org == null) return;
        if (org.getId() == null) {
            dao.saveOrUpdate(org);
        } else {
            Org o = get(org.getId());
            o.setIconFa(org.getIconFa());
            o.setName(org.getName());
            o.setSname(org.getSname());
            o.setSort(org.getSort());
            o.setStatus(org.getStatus());
            o.setType(org.getType());
        }
    }

}
