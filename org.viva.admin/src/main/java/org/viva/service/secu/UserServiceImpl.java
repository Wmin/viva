package org.viva.service.secu;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.secu.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private BaseDAO<User> dao;

	@Override
	public User getUserByName(String username) {
		return dao.get("from User where code = ?", new String[] { username });
	}

	@Override
	public List<String> loadUserAuthoritiesByName(String username) {
		return null;
	}

	public void setDao(BaseDAO<User> dao) {
		this.dao = dao;
	}

}
