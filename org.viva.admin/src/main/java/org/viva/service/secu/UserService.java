package org.viva.service.secu;

import java.util.List;

import org.viva.entity.secu.User;

public interface UserService {

	User getUserByName(String username);

	List<String> loadUserAuthoritiesByName(String username);

}
