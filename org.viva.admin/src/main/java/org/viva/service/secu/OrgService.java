package org.viva.service.secu;

import java.util.List;

import org.viva.entity.secu.Org;

public interface OrgService {

	List<?> getAllMap();

	Org get(long id);
	
	void save(Org org);

}
