package org.viva.service.secu;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.secu.Menu;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

    @Resource
    private BaseDAO<Menu> dao;

    @Override
    public List<?> getAdminMenuAll() {
        return dao.find("from Menu where type='server' order by pid,sort");
    }

    @Override
    public List<?> getMenuAll() {
        return dao.find("from Menu order by pid,sort");
    }

    @Override
    public Menu get(long id) {
        return dao.get(Menu.class, id);
    }

    @Override
    public void save(Menu menu) {
        if (menu == null) return;
        if (menu.getId() == null) {
            dao.save(menu);
        } else {
            Menu m = get(menu.getId());
            m.setIconFa(menu.getIconFa());
            m.setName(menu.getName());
            m.setSort(menu.getSort());
            m.setStatus(menu.getStatus());
            m.setUrl(menu.getUrl());
            m.setType(menu.getType());
        }
    }

    @Override
    public void del(Menu menu) {
        dao.delete(menu);
    }

}
