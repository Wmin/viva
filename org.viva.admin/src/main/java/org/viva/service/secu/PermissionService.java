package org.viva.service.secu;

import java.util.List;

import org.viva.entity.secu.Permission;

public interface PermissionService {

	List<Permission> findPermByUri(String url);

}
