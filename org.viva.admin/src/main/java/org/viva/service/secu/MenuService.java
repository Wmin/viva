package org.viva.service.secu;

import java.util.List;

import org.viva.entity.secu.Menu;

public interface MenuService {

	List<?> getMenuAll();

	Menu get(long id);

    void save(Menu menu);

    void del(Menu menu);

    List<?> getAdminMenuAll();

}
