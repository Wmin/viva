package org.viva.service.oa;

import java.util.Date;
import java.util.List;

import org.viva.entity.oa.Calendar;

public interface CalendarService {

	List<Calendar> getList(Date startDate, Date endDate);

}
