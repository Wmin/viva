package org.viva.service.oa;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.oa.Calendar;

@Service("calendarService")
public class CalendarServiceImpl implements CalendarService {

	@Resource
	private BaseDAO<Calendar> dao;

	@SuppressWarnings("unchecked")
	@Override
	public List<Calendar> getList(Date startDate, Date endDate) {
		Criteria criteria = dao.getCurrentSession().createCriteria(Calendar.class);
		criteria.add(Restrictions
				.and(Restrictions.or(Restrictions.between("startDate", startDate, endDate), Restrictions.between("endDate", startDate, endDate))));
		return criteria.list();
	}

}
