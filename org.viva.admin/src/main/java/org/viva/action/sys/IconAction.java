package org.viva.action.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IconAction {

    @RequestMapping("/sys/icon/choose")
    public String choose() {
        return "/sys/icon/icon_choose";
    }

}
