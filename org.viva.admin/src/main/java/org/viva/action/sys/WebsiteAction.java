package org.viva.action.sys;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.entity.sys.Website;
import org.viva.service.sys.WebsiteService;

@Controller
@RequestMapping("/sys/website")
public class WebsiteAction {

	@Resource
	private WebsiteService websiteService;

	@RequestMapping()
	@RequiresAuthentication
	public String index(HttpServletRequest request) {
		request.setAttribute("list", websiteService.getAll());
		return "/sys/website/website";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@RequiresAuthentication
	@ResponseBody
	public String doUpdate(HttpServletRequest request) {
		Website website = websiteService.get(Long.parseLong(request.getParameter("pk") + ""));
		website.setString_(request.getParameter("value"));
		websiteService.rfCache();
		return "success";
	}

}
