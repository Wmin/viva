package org.viva.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AboutAction {

    @RequestMapping("/about")
    public String index() {
        return "about";
    }

    @RequestMapping("/readme")
    public String readme() {
        return "readme";
    }
}
