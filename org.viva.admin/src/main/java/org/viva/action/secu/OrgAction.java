package org.viva.action.secu;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.core.api.Ajax;
import org.viva.entity.secu.Org;
import org.viva.service.secu.OrgService;

@Controller
public class OrgAction {

    @Resource
    private OrgService orgService;

    @RequestMapping("/secu/org")
    @RequiresAuthentication
    public String index(HttpServletRequest request) {
        return "/secu/org";
    }

    @RequestMapping("/secu/org/allJson")
    @ResponseBody
    public Map<String, Object> allJson(HttpServletRequest request) {
        return Ajax.exec(orgService.getAllMap());
    }

    @RequestMapping("/secu/org/infoJson")
    @ResponseBody
    public Org infoJson(long id) {
        return orgService.get(id);
    }

    @RequestMapping("/secu/org/save")
    @ResponseBody
    public Map<String, Object> doSave(Org org) {
        orgService.save(org);
        return Ajax.exec();
    }

}
