package org.viva.action.secu;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.core.api.Ajax;
import org.viva.entity.secu.Menu;
import org.viva.service.secu.MenuService;

@Controller
public class MenuAction {

    @Resource
    private MenuService menuService;

    @RequestMapping("/secu/menu")
    @RequiresAuthentication
    public String index(HttpServletRequest request) {
        return "/secu/menu";
    }

    @RequestMapping("/secu/menu/allJson")
    @ResponseBody
    public Map<String, Object> allJson(HttpServletRequest request) {
        return Ajax.exec(menuService.getMenuAll());
    }

    @RequestMapping("/secu/menu/infoJson")
    @ResponseBody
    public Menu infoJson(long id) {
        return menuService.get(id);
    }

    @RequestMapping("/secu/menu/save")
    @ResponseBody
    public Map<String, Object> doSave(Menu menu) {
        menuService.save(menu);
        return Ajax.exec(menu);
    }
    
    @RequestMapping("/secu/menu/del")
    @ResponseBody
    public Map<String, Object> doDel(Menu menu) {
        menuService.del(menu);
        return Ajax.exec();
    }

}
