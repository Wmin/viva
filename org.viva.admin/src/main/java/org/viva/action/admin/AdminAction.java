package org.viva.action.admin;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.core.api.Ajax;
import org.viva.secu.Security;
import org.viva.service.secu.MenuService;
import org.viva.service.sys.WebsiteService;

@Controller
public class AdminAction {

    @Resource
    private MenuService    menuService;
    @Resource
    private WebsiteService websiteService;

    @RequestMapping("/admin")
    @RequiresAuthentication
    public String index(HttpServletRequest request, ModelMap model) {
        model.put("user", Security.getUser());
        model.put("website", websiteService.getCache());
        return "/admin/index";
    }

    @RequestMapping("/admin/menu")
    @ResponseBody
    public Map<String, Object> menu(HttpServletRequest request) {
        return Ajax.exec(menuService.getAdminMenuAll());
    }

}
