package org.viva.action.oa;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.api.ActionUtils;
import org.viva.core.util.UTIL;
import org.viva.entity.oa.Calendar;
import org.viva.service.oa.CalendarService;

@Controller
public class CalendarAction {

	@Resource()
	private CalendarService calendarService;

	@RequestMapping(value = "/oa/calendar", method = RequestMethod.GET)
	public String index(HttpServletRequest request) {
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.getSession();
		return "/oa/calendar";
	}

	@RequestMapping(value = "/oa/calendar/add", method = RequestMethod.GET)
	public String add(HttpServletRequest request, ModelMap model) {
		model.put("param", ActionUtils._getParameters());
		return "/oa/calendar_add";
	}

	@RequestMapping(value = "/oa/calendar/list", method = RequestMethod.GET)
	@ResponseBody
	public List<Calendar> list(HttpServletRequest request, ModelMap model, String startDate, String endDate) throws Exception {
		Date sd = UTIL.FORMAT.toDate(startDate, "yyyy-MM-dd HH:mm");
		Date ed = UTIL.FORMAT.toDate(endDate, "yyyy-MM-dd HH:mm");
		return calendarService.getList(sd, ed);
	}

	@RequestMapping(value = "/oa/calendar/add", method = RequestMethod.POST)
	public String doAdd(HttpServletRequest request) {
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.getSession();
		return "/oa/calendar";
	}
}
