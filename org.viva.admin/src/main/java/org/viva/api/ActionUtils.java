package org.viva.api;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.viva.core.Web;

public class ActionUtils {

	public static void initModer() {

	}

	public static Map<String, Object> _getParameters() {
		Map<String, Object> m = new HashMap<String, Object>();
		Enumeration<?> en = Web.getRequest().getParameterNames();
		while (en.hasMoreElements()) {
			Object enN = en.nextElement();
			String para = Web.getRequest().getParameter(enN.toString()).trim();
			m.put(enN.toString(), "undefined".equals(para) ? "" : para.trim());
		}
		return m;
	}

}
