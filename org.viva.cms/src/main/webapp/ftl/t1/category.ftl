<!DOCTYPE html>
<html class="not-ie" lang="en">
<head>
<title>${category.name} - 邋遢熊DOWDYBEAR</title>
<meta name="description" content="${category.memo} - ${category.name} - 邋遢熊">
<meta name="keywords" content="邋遢熊, ${category.name}">
<#include "/${website.cms_respath}/_importBase.ftl">
</head>
<body>
	<!-- Header -->
	<#include "/${website.cms_respath}/_head.ftl">
	<!-- Header End -->

	<!-- Content -->
	<div id="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="a_h a_title"> ${category.name} <small>${category.memo}</small> </h2>
				</div>
				<div class="col-xs-9">
					<@cms_content_page cgid=category.id>
					  <#list list as item>
					  	<div class="row">
							<div class="col-xs-12">
								<a href="blog-detail.html"><img src="" alt=""></a>
								<div class="row">
									<div class="col-xs-12 post-d-info">
									<h3><a style="color: white;" href="${basePath}/a/${item.id}">${item.title}</a></h3>
										<div class="blue-dark">
											<i class="fa fa-user"></i> By ${item.user.code}
											<i class="fa fa-tag"></i>
											${item.tags}
											<i class="fa fa-comment"></i> <span id="sourceId::article$item.ID" class="cy_cmt_count"></span>
											<i class="fa fa-eye"></i>${item.viewCount} <i class="fa fa-clock-o"></i>${item.createDate?string("yyyy-MM-dd")}
										</div>
										<p>${item.intro}</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row space40"></div>
					  </#list>
					</@cms_content_page>
					<script id="cy_cmt_num" src="http://changyan.sohu.com/upload/plugins/plugins.list.count.js?clientId=cyrv6VH1d"></script>
					<div class="row space30"></div>
					<!-- Paging -->
					<div class="row">
						<div class="col-xs-9">
							<a href="#" class="paging">&#62;</a> <a href="#" class="paging">84</a> <a href="#" class="paging">83</a> <a href="#" class="paging">82</a> <a
								href="#" class="paging">...</a> <a href="#" class="paging">3</a> <a href="#" class="paging">2</a> <a href="#" class="paging">1</a> <a href="#"
								class="paging">&#60;</a>
						</div>
					</div>
					<!-- Paging End -->

					<div class="row space40"></div>

				</div>

				<!-- Side Bar -->
				<div class="col-xs-3 rightbar">

					<h3 class="p-t-0">Search</h3>
					<div class="search-box">
						<script type="text/javascript">(function(){document.write(unescape('%3Cdiv id="bdcs"%3E%3C/div%3E'));var bdcs = document.createElement('script');bdcs.type = 'text/javascript';bdcs.async = true;bdcs.src = 'http://znsv.baidu.com/customer_search/api/js?sid=15524494393292278378' + '&plate_url=' + encodeURIComponent(window.location.href) + '&t=' + Math.ceil(new Date()/3600000);var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(bdcs, s);})();</script>
					</div>
					<h3>Categories</h3>
					<ul class="list-c">
						<li><i class="icon-chevron-right"></i><a href="#">Business Plan</a></li>
						<li><i class="icon-chevron-right"></i><a href="#">Entertainment</a></li>
						<li><i class="icon-chevron-right"></i><a href="#">News & Politics</a></li>
						<li><i class="icon-chevron-right"></i><a href="#">Social Media Networks</a></li>
						<li><i class="icon-chevron-right"></i><a href="#">Technology & Innovation</a></li>
					</ul>

					<h3>Tags</h3>
					#foreach($item in $!{J.v('tags')})
					#set($tempurl = "/cms/Tag!load.action?TAG="+ ${item.NAME})
					<a href="$_basePath$response.encodeUrl($tempurl)"><div class="tag">${item.NAME}(${item.ARTICLE_COUNT})</div></a>
					#end

					<div class="row space50"></div>
					<div id="hm_t_72463"></div>
					<script>document.write(unescape('%3Cdiv id="hm_t_72463"%3E%3C/div%3E%3Cscript charset="utf-8" src="http://crs.baidu.com/t.js?siteId=05f9434b9a47aab0df8ee0b795bf5468&planId=72463&async=0&referer=') + encodeURIComponent(document.referrer) + '&title=' + encodeURIComponent(document.title) + '&rnd=' + (+new Date) + unescape('"%3E%3C/script%3E'));</script>
				</div>
			</div>
		</div>
	</div>
	<!-- Content End -->

	<#include "/${website.cms_respath}/_foot.ftl">

	<!-- JavaScripts -->
	<script type="text/javascript" src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
	<script type="text/javascript" src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${basePath}/res/js/functions.js"></script>
	<script type="text/javascript" defer src="${basePath}/res/js/jquery.flexslider.js"></script>

</body>
</html>
