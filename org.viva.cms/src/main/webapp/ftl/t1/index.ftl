<!DOCTYPE html>
<html class="not-ie" lang="en">
<head>
<title>${website.ws_name}</title>
<meta name="title" content="邋遢熊 - DOWDYBEAR">
<meta name="keywords" content="邋遢熊, dowdybear, java, oracle, bootstrap, creative, design">
<meta name="description" content="微信编辑器, java, oracle, bootstrap, creative, design,邋遢熊, dowdybear,">
<#include "/t1/_importBase.ftl">
<link rel="stylesheet" href='${basePath}/res/${website.cms_respath}/css/flexslider.css' type="text/css" media="screen">
<style type="text/css">
h1, h2, h3, h4, h5, h6 {
	position: relative;
	font-family: "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight: 200;
	line-height: 1.25;
	color: #fff;
	margin: 0px;
	padding: 20px 0;
	clear: both;
}

h1, h2 {
	font-size: 42px;
	padding: 20px 0 10px 0;
}

h1 {
	color: #3b3b3b;
}

h2 {
	padding: 50px 0 40px 0;
}

h3 {
	font-size: 24px;
	line-height: 30px;
}

h4 {
	font-size: 20px;
	line-height: 24px;
	font-weight: 500;
}

h5, h6 {
	font-size: 20px;
	line-height: 26px;
}
</style>
</head>
<body>
	<!-- Header -->
	<header id="header">

		<div class="container">
			<div class="row t-container">
				<div class="texture">
					<img src="${basePath}/res/${website.cms_respath}/img/background.png" alt="">
				</div>
				<!-- Logo -->
				<div class="col-xs-3">
					<div class="logo" style="background-color: black;">
						<a href="${basePath}" ><img src="${basePath}/res/${website.cms_respath}/logo-home.png" alt="" /></a>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="space90"></div>
						<#include "/t1/_menu.ftl">
					<div class="space15 hidden-phone"></div>
				</div>
				<div class="col-xs-4">
				</div>
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-4">
							<h1>
								We are <strong>DOWDYBEAR</strong>
							</h1>
							<div class="head-info">
								我们拥有完整的项目团队.<br>
								我们提供全面的技术分享.<br>
								我们不定期的举办交流活动. <br>
								想要加入我们么?
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="space60"></div>
			<div class="col-xs-12">
				<!-- Slider -->
				<div class="slider1 flexslider">
					<ul class="slides">
						<li><img src="${basePath}/res/${website.cms_respath}/img/slider/1.jpg" alt=""></li>
						<li><img src="${basePath}/res/${website.cms_respath}/img/slider/2.jpg" alt=""></li>
						<li><img src="${basePath}/res/${website.cms_respath}/img/slider/3.jpg" alt=""></li>
					</ul>
				</div>
				<!-- Slider End -->
			</div>
		</div>
	</header>

	<div class="space80"></div>

	<!-- Content -->
	<!-- Footer -->
	<#include "/${website.cms_respath}/_foot.ftl">
	<!-- Footer End -->

	<!-- JavaScripts -->
	<script type="text/javascript" src="${basePath}/res/${website.cms_respath}/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="${basePath}/res/${website.cms_respath}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${basePath}/res/${website.cms_respath}/js/functions.js"></script>
	<script type="text/javascript" src="${basePath}/res/${website.cms_respath}/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" defer src="${basePath}/res/${website.cms_respath}/js/jquery.flexslider.js"></script>

</body>
</html>
