<#assign basePath=request.contextPath>
<meta name="baidu-site-verification" content="6b5ed30f1f94c0e3dd8e3492b29a540e" />
<meta name="baidu-site-verification" content="037SMq2tO8" />
<meta name="baidu_union_verify" content="818cf428b262e86ba114cc3689bf982e"><!-- 百度联盟 -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="${basePath}/img/favicon.png" rel="icon" type="image/png">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="${basePath}/res/${website.cms_respath}/css/styles.css">
<script>
	var _hmt = _hmt || [];
	(function() {
		var hm = document.createElement("script");
		hm.src = "//hm.baidu.com/hm.js?ae308215e3fdcb2b859656fdef1b17fa";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(hm, s);
	})();
</script>
