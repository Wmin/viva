<!DOCTYPE html>
<html class="not-ie" lang="en">
<head>
	<title>${data.title} - 邋遢熊</title>
	<meta name="description" content="${data.intro} - ${data.title} - 邋遢熊">
	<meta name="keywords" content="邋遢熊, ${data.title}">
	<#include "/${website.cms_respath}/_importBase.ftl">
	<script src="http://cdn.bootcss.com/jquery/2.1.3/jquery.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="http://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/shCore.min.js"></script>
	<script src="http://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/shAutoloader.min.js"></script>
	<link href="http://cdn.bootcss.com/SyntaxHighlighter/3.0.83/styles/shCoreDefault.min.css" rel="stylesheet">
	<script type="text/javascript">
	$(function(){
		function path() {
		    var args = arguments, result = [];
		    for (var i = 0; i < args.length; i++){
		        result.push(args[i].replace('@', 'http://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/'));
		    }
		    return result
		};
		SyntaxHighlighter.autoloader.apply(null, path(
		        'applescript            @shBrushAppleScript.js',
		        'actionscript3 as3      @shBrushAS3.js',
		        'bash shell             @shBrushBash.js',
		        'coldfusion cf          @shBrushColdFusion.js',
		        'cpp c                  @shBrushCpp.js',
		        'c# c-sharp csharp      @shBrushCSharp.js',
		        'css                    @shBrushCss.js',
		        'delphi pascal          @shBrushDelphi.js',
		        'diff patch pas         @shBrushDiff.js',
		        'erl erlang             @shBrushErlang.js',
		        'groovy                 @shBrushGroovy.js',
		        'java                   @shBrushJava.js',
		        'jfx javafx             @shBrushJavaFX.js',
		        'js jscript javascript  @shBrushJScript.js',
		        'perl pl                @shBrushPerl.js',
		        'php                    @shBrushPhp.js',
		        'text plain             @shBrushPlain.js',
		        'py python              @shBrushPython.js',
		        'ruby rails ror rb      @shBrushRuby.js',
		        'sass scss              @shBrushSass.js',
		        'scala                  @shBrushScala.js',
		        'sql                    @shBrushSql.js',
		        'vb vbnet               @shBrushVb.js',
		        'xml xhtml xslt html    @shBrushXml.js')
		 );
	
		SyntaxHighlighter.all();
	});
	</script>
<style type="text/css">
.content {
	border-radius: 5px;
	border: 1px solid #CCCCCC;
	background-color: #fff;
	-webkit-box-shadow: none;
	padding: 45px 15px 15px;
	-webkit-box-shadow: inset 0 3px 6px rgba(0, 0, 0, .05);
	box-shadow: inset 0 3px 6px rgba(0, 0, 0, .05);
	box-sizing: border-box;
}
.syntaxhighlighter td.code .container::before, .syntaxhighlighter td.code .container::after {display: none;}
</style>
</head>
<body>
	<!-- Header -->
	<#include "/${website.cms_respath}/_head.ftl">
	<!-- Header End -->

	<!-- Content -->
	<div id="content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="a_h a_title">${data.title}</h2>
				</div>
				<div class="col-xs-9">
					<!-- Blog Detail Content -->
					<div class="row">
						<div class="col-xs-12 content">
							<div class="post-d-info">
								<div class="blue-dark">
									<i class="fa fa-user"></i> By Admin <i class="fa fa-tag"></i>
									${data.tags}
									<i class="fa fa-comment"></i> <span id="sourceId::article${data.id}" class="cy_cmt_count" ></span>
									<i class="fa fa-eye"></i> ${data.viewCount}
								</div>
								<hr />
								<div>${data.content}</div>
							</div>
							<hr />
							<div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a></div>
							<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"24"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
							<hr />
							<!--高速版-->
							<div id="SOHUCS" sid="article${data.id}"></div>
							<script charset="utf-8" type="text/javascript" src="http://changyan.sohu.com/upload/changyan.js" ></script>
							<script type="text/javascript">
							    window.changyan.api.config({
							        appid: 'cyrv6VH1d',
							        conf: 'prod_88c3dfd22c63db442e9da93b5109b162'
							    });
							</script>
						</div>
					</div>
					<!-- Blog Detail Content End -->
				</div>

				<!-- Side Bar -->
				<div class="col-xs-3 rightbar">

					<h3 class="p-t-0">Search</h3>
					<div class="search-box">
						<script type="text/javascript">(function(){document.write(unescape('%3Cdiv id="bdcs"%3E%3C/div%3E'));var bdcs = document.createElement('script');bdcs.type = 'text/javascript';bdcs.async = true;bdcs.src = 'http://znsv.baidu.com/customer_search/api/js?sid=15524494393292278378' + '&plate_url=' + encodeURIComponent(window.location.href) + '&t=' + Math.ceil(new Date()/3600000);var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(bdcs, s);})();</script>
					</div>
					<script type="text/javascript">
					    var cpro_id = "u2345041";
					</script>
					<script src="http://cpro.baidustatic.com/cpro/ui/c.js" type="text/javascript"></script>
					<h3>Tags 标签</h3>
					#foreach($item in $!{J.v('tags')})
					#set($tempurl = "/cms/Tag!load.action?TAG="+ ${item.NAME})
					<a href="$_basePath$response.encodeUrl($tempurl)"><div class="tag">${item.NAME}(${item.ARTICLE_COUNT})</div></a>
					#end
					<div style="clear: both;"></div>
					<h3>链接</h3>
					<!-- 代码1：放在页面需要展示的位置  -->
					<!-- 如果您配置过sourceid，建议在div标签中配置sourceid、cid(分类id)，没有请忽略  -->
					<div id="cyWallvertical" role="cylabs" data-use="wallvertical"></div>
					<!-- 代码2：用来读取评论框配置，此代码需放置在代码1之后。 -->
					<!-- 如果当前页面有评论框，代码2请勿放置在评论框代码之前。 -->
					<!-- 如果页面同时使用多个实验室项目，以下代码只需要引入一次，只配置上面的div标签即可 -->
					<!-- 如需修改默认配置，请到changyan.sohu.com后台进行配置 -->
					<script type="text/javascript" charset="utf-8" src="http://changyan.itc.cn/js/??lib/jquery.js,changyan.labs.js?appid=cyrv6VH1d"></script>
					<div class="row space30"></div>
					<div id="hm_t_72463"></div>
					<script>document.write(unescape('%3Cdiv id="hm_t_72463"%3E%3C/div%3E%3Cscript charset="utf-8" src="http://crs.baidu.com/t.js?siteId=05f9434b9a47aab0df8ee0b795bf5468&planId=72463&async=0&referer=') + encodeURIComponent(document.referrer) + '&title=' + encodeURIComponent(document.title) + '&rnd=' + (+new Date) + unescape('"%3E%3C/script%3E'));</script>
					<div class="row space50"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Content End -->
		
	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-6">
					<div class="logo-footer">
						<a href="index-2.html"><img src="${basePath}/res/${website.cms_respath}/img/logo-footer.png" alt=""></a> CORPORATE TEMPLATE
					</div>
				</div>
				<div class="col-xs-6 right">
				<script type="text/javascript">
				var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
				document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F05f9434b9a47aab0df8ee0b795bf5468' type='text/javascript'%3E%3C/script%3E"));
				</script>
					&copy; 2015 DOWDYBEAR. All rights reserved. 
				</div>
			</div>
	
		</div>
	</footer>

	<!-- JavaScripts -->
	<script id="cy_cmt_num" src="http://changyan.sohu.com/upload/plugins/plugins.list.count.js?clientId=cyrv6VH1d"></script>
</body>
</html>
