<nav class="navbar">
	<!-- Menu -->
	<ul class="nav">
		<li><a href="${basePath}/">Home</a></li>
		<li><a href="${basePath}/wx/edit">微信编辑器</a></li>
		<@cms_menu>
		  <#list menus as menu>
		    <li><a <#if (menu.children?size==0)> href='${basePath}/g/${menu.id}'</#if> >${menu.name}</a>
				<#if (menu.children?size>0)>
			    	<ul>
			    		<#list menu.children as mi>
						<li><a href="${basePath}/g/${mi.id}">${mi.name}</a></li>
			  			</#list>
					</ul>
			    </#if>
			</li>
		  </#list>
		</@cms_menu>
		<@cms_user>
			<#if user??>
				<li><a href="javascript:void(0)">${user.code}</a></li>
			<#else>
				<li><a href="${basePath}/login">登录</a></li>
			</#if>
		</@cms_user>
	</ul>
</nav>
