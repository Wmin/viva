<!-- Footer -->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 pos-r">
				<h2 class="a_h a_title">Get to know us completely</h2>
				<div class="texture-footer">
					<img src="$_basePath/img/background-footer.png" alt="">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-5">
				<p>OT Porta montes montes vel nunc cras porttitor mid elementum dictumst dignissim dapibus urna ac, enim?</p>
				<h3 class="a_h">Contact Form</h3>
				<div>
					<form class="form-main" name="ajax-form" id="ajax-form" action="http://www.entiri.com/minett/php/mail-it.php" method="post">
						<div id="ajaxsuccess">E-mail was successfully sent.</div>
						<div class="error" id="err-name">Please enter name</div>
						<input name="name" id="name" type="text" value="Name" onfocus="if(this.value == 'Name') this.value='';"
							onblur="if(this.value == '') this.value='Name';">

						<div class="error" id="err-email">Please enter e-mail</div>
						<div class="error" id="err-emailvld">E-mail is not a valid format</div>
						<input name="email" id="email" type="text" value="E-mail" onfocus="if(this.value == 'E-mail') this.value='';"
							onblur="if(this.value == '') this.value='E-mail';">

						<div class="error" id="err-message">Please enter message</div>
						<textarea name="message" id="message" onfocus="if(this.value == 'Message') this.value='';" onblur="if(this.value == '') this.value='Message';">Message</textarea>
						<br>
						<div>
							<div class="error" id="err-form">There was a problem validating the form please check!</div>
							<div class="error" id="err-timedout">The connection to the server timed out!</div>
							<div class="error" id="err-state"></div>
						</div>
						<button id="send" class="btn f-right">
							Send Message <i class="fa fa-check"></i>
						</button>
					</form>
				</div>

				<div class="row space40"></div>

				<a href="#" class="social-network sn2 behance"></a> <a href="#" class="social-network sn2 facebook"></a> <a href="#"
					class="social-network sn2 pinterest"></a> <a href="#" class="social-network sn2 flickr"></a> <a href="#" class="social-network sn2 dribbble"></a>
				<a href="#" class="social-network sn2 lastfm"></a> <a href="#" class="social-network sn2 forrst"></a> <a href="#" class="social-network sn2 xing"></a>
			</div>
			<div class="col-xs-3 col-xs-offset-3">
				<div class="space80"></div>
				<div class="space70"></div>
				We have a dream ! <br> <br> 
				<i class="fa fa-qq"></i>技术交流群： 271540642<br> 
				<i class="fa fa-envelope"></i><a href="mailto:lichao@dowdybear.com">lichao@dowdybear.com</a><br> 
				<i class="fa fa-home"></i><a href="http://www.dowdybear.com">www.dowdybear.com</a>

			</div>
		</div>
		<div class="row space50"></div>
		<div class="row">
			<div class="col-xs-6">
				<div class="logo-footer">
					<a href="$_basePath"><img src="${basePath}/res/${website.cms_respath}/img/logo-footer.png" alt=""></a> CORPORATE TEMPLATE
				</div>
			</div>
			<div class="col-xs-6 right">
			<script type="text/javascript">
			var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
			document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F05f9434b9a47aab0df8ee0b795bf5468' type='text/javascript'%3E%3C/script%3E"));
			</script>
				&copy; 2015 DOWDYBEAR. All rights reserved.
			</div>
		</div>

	</div>
</footer>
