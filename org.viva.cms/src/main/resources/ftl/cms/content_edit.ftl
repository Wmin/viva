<!DOCTYPE html>
<html lang="en">
<head>
<#include "/admin/common/app.ftl">
<script type="text/javascript" src="http://cdn.bootcss.com/jquery.form/3.51/jquery.form.min.js"></script>
<script type="text/javascript" src="${basePath}/res/cdn/jQuery-tagEditor-master/jquery.tag-editor.min.js"></script>
<script type="text/javascript" src="${basePath}/res/cdn/jQuery-tagEditor-master/jquery.caret.min.js"></script>
<link rel="stylesheet" href="${basePath}/res/cdn/jQuery-tagEditor-master/jquery.tag-editor.css">
<link href="//cdn.bootcss.com/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<script src="//cdn.bootcss.com/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap-datepicker/1.4.1/locales/bootstrap-datepicker.zh-CN.min.js"></script>
<script src="${basePath}/res/plugins/ueditor143/ueditor.config.js"></script>
<script src="${basePath}/res/plugins/ueditor143/ueditor.all.min.js"></script>
<script src="${basePath}/res/plugins/ueditor143/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
	$(function() {
		$("#confForm").submit(function() {
			$("#confForm").ajaxSubmit({
				dataType : "json",
				success : function(json) {
					if (json.flag) {
						alert("操作成功");
						window.location.href = "${basePath}/cms/content/page";
					} else {
						alert(json.msg);
					}
				}
			});
			return false;
		});

		var ue = UE.getEditor('content');
		$('[name=tags]').tagEditor();
		$(".tagsinput").css("width", "100%");
		$(".datepicker").datepicker({format:"yyyy-mm-dd",language:"zh-CN"});
	});
</script>
</head>
<body class="container-fluid">
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">文章编辑</h1>
			<p class="description">Plain text boxes, select dropdowns and other basic form elements</p>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-default">
			<form id="confForm" role="form" method="post" action="${basePath}/cms/content/save">
				<input autocomplete="off" name="id" value="${data.id}" type="hidden" />
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label for=title class="control-label">标题</label>
							<div>
								<input autocomplete="off" class="form-control" id="title" name="title" value="${data.title}" data-validate="required" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">正文</label>
							<div>
								<script id="content" name="content" type="text/plain" style="width: 100%; height: 300px;">${data.content}</script>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">标签</label>
							<div class="col-sm-10">
								<input class="form-control" name="tags" value="${data.tags}" />
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div data-spy="affix" data-offset-top="1">
							<div class="form-horizontal">
								<div class="form-group">
									<span class="col-sm-4"></span>
									<div class="col-sm-8">
										<button class="btn btn-success btn-icon btn-block" type="button" onclick='$("#confForm").submit();'>
											<span> 保 存 </span> <i class="fa fa-check"></i>
										</button>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="cgId">栏目</label>
									<div class="col-sm-8">
										<select class="form-control" name="cgId" id="cgId">
											<#list cglist as item>
											<option value="${item.id}" <#if data.cgId==item.id> selected="selected" </#if>>${item.name}</option> 
											</#list>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="status">状态</label>
									<div class="col-sm-8">
										<select id="status" name="status" class="form-control">
											<option value="1">1</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="A_TEMPLATE">模板</label>
									<div class="col-sm-8">
										<select id="" name="" class="form-control">
											<option value="1">1</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="A_DATE">日期</label>
									<div class="col-sm-8">
										<div class="input-group">
											<input name="createDate" class="form-control datepicker" value="${data.createDate}">
											<div class="input-group-addon">
												<a href="#"><i class="linecons-calendar"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>