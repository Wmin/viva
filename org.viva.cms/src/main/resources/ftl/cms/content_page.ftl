<!DOCTYPE html>
<html lang="cn">
<head>
<#include "/admin/common/app.ftl">
<script type="text/javascript">
	function del(id) {
		if (confirm("确认删除?")) {
			jQuery.ajax({
				url : "${basePath}/cms/content/del?ID=" + id,
				dataType : "json",
				success : function(json) {
					if (json.flag) {
						window.location.reload();
					} else {
						alert(json.msg);
					}
				}
			});
		}
	}
</script>
</head>
<body class="container-fluid">
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">文章管理</h1>
			<p class="description">Plain text boxes, select dropdowns and other basic form elements</p>
		</div>
		<div class="breadcrumb-env">
			<a class="btn btn-primary" href="${basePath}/cms/content/edit">新建</a>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-default">
			<table class="table tree">
				<thead>
					<tr class="treegrid-1">
						<th>名称</th>
						<th>日期</th>
						<th>操作</th>
					</tr>
				</thead>
				<#list page.data as item>
				<tr>
					<td>${item.title}</td>
					<td>${item.createDate}</td>
					<td><a href="${basePath}/cms/content/edit?id=${item.id}"><span class="fa fa-edit"></span></a> / <a href="javascript:void(0);"
						onclick="del(${item.id})"><span class="fa fa-trash"></span></a></td>
				</tr>
				</#list>
			</table>
			<nav>
				<ul class="pagination">
					<#if param.page &gt; 1>
					<li><a href="${basePath}/${page.purl}" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a></li>
					</#if>
					
					<#list -3..3 as c>
					<#assign temppage = param.page - c>
					<#if temppage&gt;=1 && param.maxPage &gt;= temppage>
					<li <#if param.page==temppage>class="active"</#if>>
						<a <#if param.page !=temppage> href="$_basePath/cms/Article.action?page=$temppage"</#if>>${temppage}</a></li> 
					</#if>
					</#list>
					<#if param.maxPage &gt; page.maxPage>
					<li><a href="${page.nurl}" aria-label="Next"> <span aria-hidden="true">&raquo;</span></a></li>
					</#if>
				</ul>
			</nav>
		</div>
	</div>
</body>
</html>