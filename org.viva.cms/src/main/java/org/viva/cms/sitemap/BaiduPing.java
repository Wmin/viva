package org.viva.cms.sitemap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class BaiduPing {

	public static final String BAIDU_RPC = "http://ping.baidu.com/ping/RPC2";
	public static final String GOOGLE_RPC = "http://blogsearch.google.com/ping/RPC2";

	private static String buildMethodCall(String title, String url, String shareURL, String rssURL) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
		buffer.append("<methodCall>");
		buffer.append("<methodName>weblogUpdates.extendedPing</methodName>");
		buffer.append("<params>");
		buffer.append("<param><value><string>" + url + "</string></value></param>");
		buffer.append("<param><value><string>" + title + "</string></value></param>");
		buffer.append("<param><value><string>" + shareURL + "</string></value></param>");
		buffer.append("<param><value><string>" + rssURL + "</string></value></param>");
		buffer.append("</params>");
		buffer.append("</methodCall>");
		return buffer.toString();
	}

	public static String pingBaidu(String title, String url, String shareURL, String rssURL) throws Exception {
		HttpPost post = new HttpPost(BAIDU_RPC);
		post.addHeader("User-Agent", "request");
		post.addHeader("Content-Type", "text/xml");
		String methodCall = buildMethodCall(title, url, shareURL, rssURL);
		StringEntity entity = new StringEntity(methodCall);
		post.setEntity(entity);
		HttpClient httpclient = new DefaultHttpClient();
		// httpclient.getHostConfiguration().setProxy("127.0.0.1", 8888);
		HttpResponse response = httpclient.execute(post);
		String ret = EntityUtils.toString(response.getEntity());
//		post.releaseConnection();
//		httpclient.close();
		return ret;
	}

	public static void main(String[] args) throws Exception {
		String ret = BaiduPing.pingBaidu("邋遢熊", "http://www.dowdybear.com/", "http://www.dowdybear.com/article/details/47", "");
		System.out.println(ret);
	}

}
