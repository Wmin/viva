package org.viva.macro.cms;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.viva.secu.Security;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_user")
public class UserDirective implements TemplateDirectiveModel {

    @SuppressWarnings({ "deprecation", "rawtypes" })
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        env.setVariable("user", ObjectWrapper.DEFAULT_WRAPPER.wrap(Security.getUser()));
        body.render(env.getOut());
    }

}
