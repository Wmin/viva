package org.viva.macro.cms;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.service.cms.ContentService;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_content_page")
public class ContentListDirective implements TemplateDirectiveModel {

	@Resource
	private ContentService contentService;

	@SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		env.setVariable("list", ObjectWrapper.DEFAULT_WRAPPER.wrap(contentService.getList(params)));
		body.render(env.getOut());
	}

}
