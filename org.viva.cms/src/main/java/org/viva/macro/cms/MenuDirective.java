package org.viva.macro.cms;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.service.cms.CategoryService;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_menu")
public class MenuDirective implements TemplateDirectiveModel {

	@Resource
	private CategoryService categoryService;

	@SuppressWarnings({ "deprecation", "rawtypes" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		env.setVariable("menus", ObjectWrapper.DEFAULT_WRAPPER.wrap(categoryService.getRoot()));
		body.render(env.getOut());
	}

}
