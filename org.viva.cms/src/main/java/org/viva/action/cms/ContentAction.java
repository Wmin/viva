package org.viva.action.cms;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.viva.api.ActionUtils;
import org.viva.cms.sitemap.BaiduSm;
import org.viva.core.api.Ajax;
import org.viva.entity.cms.Content;
import org.viva.service.cms.CategoryService;
import org.viva.service.cms.ContentService;
import org.viva.service.sys.WebsiteService;

@Controller
public class ContentAction {

    @Resource
    private ContentService  contentService;
    @Resource
    private CategoryService categoryService;
    @Resource
    private WebsiteService  websiteService;

    @RequestMapping(value = "/a/{id}", method = RequestMethod.GET)
    public String page(@PathVariable("id") Long id, ModelMap modelMap) {
        modelMap.put("website", websiteService.getCache());
        modelMap.put("data", contentService.get(id));
        return "t1/content";
    }

    @RequestMapping(value = "/article/details/{id}", method = RequestMethod.GET)
    public String details(@PathVariable("id") Long id, ModelMap modelMap) {
        modelMap.put("website", websiteService.getCache());
        modelMap.put("data", contentService.get(id));
        return "t1/content";
    }

    @RequestMapping(value = "/cms/content")
    public String adminPage(ModelMap modelMap) {
        modelMap.put("website", websiteService.getCache());
        Map<String, Object> param = ActionUtils._getParameters();
        modelMap.put("param", param);
        modelMap.put("page", contentService.getPage(param));
        return "cms/content_page";
    }

    /**
     * 编辑保存
     * 
     * @param modelMap
     * @param id
     * @return
     */
    @RequestMapping(value = "/cms/content/edit", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, Long id) {
        modelMap.put("website", websiteService.getCache());
        Map<String, Object> param = ActionUtils._getParameters();
        modelMap.put("param", param);
        modelMap.put("cglist", categoryService.getRoot());
        if (id != null) {
            modelMap.put("data", contentService.get(id));
        }
        return "cms/content_edit";
    }

    /**
     * 文章修改
     * 
     * @param content
     *            文章bean
     * @return {@code org.viva.core.api.Ajax.exec()} {@code @ResponseBody}
     */
    @ResponseBody
    @RequestMapping(value = "/cms/content/save", method = RequestMethod.POST)
    public Map<String, Object> doSave(Content content) {
        if (content.getId() == null) {
            contentService.add(content);
        } else {
            contentService.up(content);
        }
        BaiduSm.art(content.getId() + "");
        return Ajax.exec();
    }

    /**
     * 文章删除
     * 
     * @param id
     *            文章ID
     * @return {@code org.viva.core.api.Ajax.exec()} {@code @ResponseBody}
     */
    @ResponseBody
    @RequestMapping(value = "/cms/content/del")
    public Map<String, Object> doDel(Long id) {
        contentService.del(id);
        return Ajax.exec();
    }

}
