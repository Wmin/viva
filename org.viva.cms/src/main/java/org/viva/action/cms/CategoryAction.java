package org.viva.action.cms;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.viva.entity.cms.Category;
import org.viva.service.cms.CategoryService;
import org.viva.service.sys.WebsiteService;

@Controller
public class CategoryAction {

    @Resource
    private CategoryService categoryService;
    @Resource
    private WebsiteService  websiteService;

    @RequestMapping(value = "/g/{id}", method = RequestMethod.GET)
    public String index(@PathVariable("id") Long id, ModelMap modelMap) {
        Category category = categoryService.get(id);
        modelMap.put("website", websiteService.getCache());
        modelMap.put("category", category);
        return "t1/category";
    }

    @RequestMapping(value = "/cg/{id}", method = RequestMethod.GET)
    public String cg(@PathVariable("id") Long id, ModelMap modelMap) {
        Category category = categoryService.get(id);
        modelMap.put("website", websiteService.getCache());
        modelMap.put("category", category);
        return "t1/category";
    }

}
