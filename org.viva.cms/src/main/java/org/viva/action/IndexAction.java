package org.viva.action;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.viva.service.sys.WebsiteService;

@Controller
public class IndexAction {

	@Resource
	private WebsiteService websiteService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		modelMap.put("website", websiteService.getCache());
		return "t1/index";
	}
}
