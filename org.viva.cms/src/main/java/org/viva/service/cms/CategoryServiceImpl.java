package org.viva.service.cms;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.viva.core.dao.BaseDAO;
import org.viva.entity.cms.Category;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Resource
	private BaseDAO<Category> dao;

//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Category> getList() {
//		String sql = "SELECT * FROM CMS_CATEGORY WHERE TYPE = 'server' ORDER BY PID,SORT";
//		SQLQuery query = dao.getCurrentSession().createSQLQuery(sql);
//		return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
//	}

	@Override
	public List<Category> getAll() {
		return dao.find("from Category");
	}

	@Override
	public List<Category> getRoot() {
		return dao.find("from Category where pid = 0 order by sort");
	}

	@Override
	public Category get(Long id) {
		return dao.get(Category.class, id);
	}

}
