package org.viva.service.cms;

import java.util.List;
import java.util.Map;

import org.viva.admin.util.page.Page;
import org.viva.entity.cms.Content;

public interface ContentService {

    List<Content> getList(Map<String, Object> param);

    Content get(Long id);

    Page getPage(Map<String, Object> param);

    void up(Content content);

    void add(Content content);

    void del(Long id);
}
