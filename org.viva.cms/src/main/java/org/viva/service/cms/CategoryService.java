package org.viva.service.cms;

import java.util.List;

import org.viva.entity.cms.Category;

public interface CategoryService {

    List<Category> getAll();

    List<Category> getRoot();

    Category get(Long id);

}
